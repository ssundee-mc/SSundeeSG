package com.mcprohosting.plugins.ssundee_sg.entities;

import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.events.PlayerSpectateEvent;
import com.mcprohosting.plugins.ssundee_sg.items.SGItem;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import com.mcprohosting.plugins.ssundee_sg.utils.user.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Participant {

    private final String name;
    private final int playerId;
    private final long timeJoined;
    private String rank;
    private boolean alive;
    private boolean voted;
    private boolean tribute = false;
    private int spawnId;
    private int place;
    private int currentKills = 0;
    private ArrayList<SGItemAction> currentItemEffects = new ArrayList<SGItemAction>();
    private ArrayList<SGItem> ownedItems = new ArrayList<SGItem>();

    private static Map<String, Participant> participants;

    static {
        participants = new HashMap<>();
    }

    public Participant(String name) {
        this.name = name;
        this.playerId = DatabaseManager.getPlayerId(this.name);
        timeJoined = System.currentTimeMillis();
        rank = getRankFromDatabase();
        alive = true;
        voted = false;
        place = 0;
        spawnId = -1;
    }

    public int getPlayerId() {
        return playerId;
    }

    public int getSpawnId() {
        return spawnId;
    }

    public void setSpawnId(int spawnId) {
        this.spawnId = spawnId;
    }

    public long getTimeJoined() {
        return timeJoined;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isTribute() {
        return tribute;
    }

    public void isTribute(boolean tribute) {
        this.tribute = tribute;
    }

    public void setDead() {
        alive = false;
        PlayerSpectateEvent playerSpectateEvent = new PlayerSpectateEvent(Bukkit.getPlayer(name));
        Bukkit.getServer().getPluginManager().callEvent(playerSpectateEvent);
    }

    public String getName() {
        return name;
    }

    public boolean hasVoted() {
        return voted;
    }

    public void setVoted() {
        voted = !voted;
    }

    public void setPlace() {
        place = getAliveParticipants().size();
    }

    public int getPlace() {
        return place;
    }

    public boolean checkIfHasEffect(SGItemAction itemEffect) {
        if (currentItemEffects.size() <= 0) {
            return false;
        } else {
            for (SGItemAction effect : currentItemEffects) {
                if (effect == itemEffect) {
                    return true;
                }
            }
            return false;
        }
    }

    public void addCurrentItemEffect(SGItemAction itemEffect) {
        currentItemEffects.add(itemEffect);
    }

    public int getCurrentKills() {
        return this.currentKills;
    }

    public void setCurrentKills(int currentKills) {
        this.currentKills = currentKills;
    }

    public String getRank() {
        return rank;
    }

    private String getRankFromDatabase() {
        String rank = DatabaseManager.getRank(this.name);
        for (Rank value : Rank.values()) {
            if (value.toString().toLowerCase().equals(rank)) {
                return value.toString();
            }
        }

        return "DEFAULT";
    }

    public SGItem getOwnedItemForEffect(SGItemAction effect) {
        SGItem output = null;
        for(SGItem i : this.ownedItems) {
            if(i.action == effect) {
                output = i;
            }
        }
        return output;
    }

    public void setOwnedItems(ArrayList<SGItem> ownedItems) {
        this.ownedItems = ownedItems;
    }

    public ArrayList<SGItem> getOwnedItems() {
        return this.ownedItems;
    }

    public static void addParticipant(Participant participant) {
        participants.put(participant.getName(), participant);
    }

    public static void removeParticipant(String name) {
        participants.remove(name);
    }

    public static Participant getParticipant(String name) {
        return participants.get(name);
    }

    public static Collection<Participant> getParticipants() {
        return participants.values();
    }


    public static ArrayList<Participant> getAliveParticipants() {
        ArrayList<Participant> alive = new ArrayList<>();
        for (Participant participant : getParticipants()) {
            if (participant.isAlive() && Bukkit.getPlayer(participant.getName()) != null) {
                alive.add(participant);
            }
        }
        return alive;
    }

    public static ArrayList<String> findKickablePlayers(String rank) {
        ArrayList<Rank> lowerRanks = Rank.getLowerPriorities(rank);
        ArrayList<String> kickablePlayers = new ArrayList<String>();

        for (Rank ranks : lowerRanks) {
            boolean found = false;
            for (Participant participant : getParticipants()) {
                if (participant.rank.equalsIgnoreCase(ranks.toString())) {
                    if (isPlayerOnline(participant.getName())) {
                        kickablePlayers.add(participant.getName());
                        found = true;
                    }
                }
            }

            if (found == true) {
                break;
            }
        }

        return kickablePlayers;
    }

    public static boolean containsParticipant(String name) {
        return participants.containsKey(name);
    }

    public static boolean isPlayerOnline(String name) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }

}
