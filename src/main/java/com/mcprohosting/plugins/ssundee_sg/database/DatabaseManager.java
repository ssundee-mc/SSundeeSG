package com.mcprohosting.plugins.ssundee_sg.database;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.items.SGItem;
import com.mcprohosting.plugins.ssundee_sg.utils.LocationUtils;
import com.mcprohosting.plugins.ssundee_sg.utils.map.GameMap;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private static Database database;

    public static void init() {
        database = new Database();

        if (!(Database.setupTables() || Database.isDBSchemaValid())) {
            SsundeeSG.getPlugin().getLogger().info("Unable to create tables or validate schema. Plugin will not load until issue is fixed");
            Bukkit.getPluginManager().disablePlugin(SsundeeSG.getPlugin());
        }
    }

    public static Connection getConnection() {
        return Database.mysql.getConnection();
    }

    public static void closeConnection(Connection connection) {
        Database.mysql.closeConnection(connection);
    }

    public static Database getDatabase() {
        return database;
    }

    public static int getLastServerAdded() {
        int retVal = 0;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT id FROM server_minigame_sg_status ORDER BY id DESC LIMIT 1;");
            rs = ps.executeQuery();

            if (rs.next()) {
                retVal = rs.getInt(1);
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to obtain the latest server id");
            closeConnection(connection);
            return -1;
        }

        return retVal;
    }

    public static boolean addServer() {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_status (id, currentPlayers, maxPlayers, status) VALUES (null, ?, ?, null);");
            ps.setInt(1, Bukkit.getOnlinePlayers().length);
            ps.setInt(2, Bukkit.getMaxPlayers());

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to add a server to the database.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean reAddServer() {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_status (id, currentPlayers, maxPlayers, status) VALUES (?, ?, ?, null);");
            ps.setInt(1, SsundeeSG.grabConfig().settings_serverid);
            ps.setInt(2, Bukkit.getOnlinePlayers().length);
            ps.setInt(3, Bukkit.getMaxPlayers());

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to re-add a server to the database.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean checkServer() {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT id FROM server_minigame_sg_status WHERE id = ?;");
            ps.setInt(1, SsundeeSG.grabConfig().settings_serverid);
            rs = ps.executeQuery();

            retVal = rs.next();


            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to check server id");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean updateStatus() {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE server_minigame_sg_status SET currentPlayers=?, maxPlayers=?, status=? WHERE id=?;");
            ps.setInt(1, Bukkit.getOnlinePlayers().length);
            ps.setInt(2, Bukkit.getMaxPlayers());
            ps.setString(3, SsundeeSG.getPlugin().getGame().getStatus().toString());
            ps.setInt(4, SsundeeSG.grabConfig().settings_serverid);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean containsDeathmatchMap(String map) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT name FROM server_minigame_sg_dm_maps WHERE name = ?;");
            ps.setString(1, map);
            rs = ps.executeQuery();

            retVal = rs.next();

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            SsundeeSG.getPlugin().getLogger().info("Failed to check if the database contains a deathmatch map.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean containsMap(String map) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT name FROM server_minigame_sg_maps WHERE name = ?;");
            ps.setString(1, map);
            rs = ps.executeQuery();

            retVal = rs.next();

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to check if the database contains a map.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean addDeathmatchMap(String map, String location, int radius) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_dm_maps (id, name, center, radius) VALUES (null, ?, ?, ?);");
            ps.setString(1, map);
            ps.setString(2, location);
            ps.setInt(3, radius);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            SsundeeSG.getPlugin().getLogger().info("Failed to add a deathmatch map to the database.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean addMap(String map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_maps (id, name, center) VALUES (null, ?, ?);");
            ps.setString(1, map);
            ps.setString(2, location);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to add a map to the database.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static ArrayList<GameMap> getDeathmatchMaps() {
        ArrayList<GameMap> maps = new ArrayList<GameMap>();

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT id, name, center, radius FROM server_minigame_sg_dm_maps;");
            rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Location location = LocationUtils.StringToLocation(rs.getString("center"));
                int radius = rs.getInt("radius");
                GameMap gm = new GameMap(id, name, location, radius);
                maps.add(gm);
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            SsundeeSG.getPlugin().getLogger().info("Failed to get maps from the database.");
            closeConnection(connection);
            return null;
        }
        closeConnection(connection);
        return maps;
    }

    public static ArrayList<GameMap> getMaps() {
        ArrayList<GameMap> maps = new ArrayList<GameMap>();

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT id, name, center FROM server_minigame_sg_maps;");
            rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Location location = LocationUtils.StringToLocation(rs.getString("center"));
                GameMap gm = new GameMap(id, name, location);
                maps.add(gm);
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get maps from the database.");
            closeConnection(connection);
            return null;
        }
        closeConnection(connection);
        return maps;
    }

    public static int getMapId(String map) {
        int id = -1;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT id FROM server_minigame_sg_maps WHERE name = ?");
            ps.setString(1, map);
            rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get a maps id from the database.");
            closeConnection(connection);
            return -1;
        }
        closeConnection(connection);
        return id;
    }

    public static boolean containsSpawn(int map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT location FROM server_minigame_sg_map_spawns WHERE map = ? AND location = ?;");
            ps.setInt(1, map);
            ps.setString(2, location);
            rs = ps.executeQuery();

            retVal = rs.next();

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to check if the database contains a spawn for a map.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean addSpawn(int map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_map_spawns (map, location) VALUES (?, ?);");
            ps.setInt(1, map);
            ps.setString(2, location);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to add a spawn to the database.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static ArrayList<String> getSpawns(int map) {
        ArrayList<String> spawns = new ArrayList<String>();

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT location FROM server_minigame_sg_map_spawns WHERE map = ?;");
            ps.setInt(1, map);
            rs = ps.executeQuery();

            while (rs.next()) {
                spawns.add(rs.getString("location"));
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get spawns for a map from the database.");
            closeConnection(connection);
            return null;
        }
        closeConnection(connection);
        return spawns;
    }

    public static boolean deleteSpawn(int map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("DELETE FROM server_minigame_sg_map_spawns WHERE map = ? AND location = ?;");
            ps.setInt(1, map);
            ps.setString(2, location);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to delete a spawn for a map from the database.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean containsChest(int map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT location FROM server_minigame_sg_map_chests WHERE map = ? AND location = ?;");
            ps.setInt(1, map);
            ps.setString(2, location);
            rs = ps.executeQuery();

            retVal = rs.next();

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to check if the database contains a spawn for a map.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean addChest(int map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_map_chests (map, location) VALUES (?, ?);");
            ps.setInt(1, map);
            ps.setString(2, location);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to add a spawn to the database.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static ArrayList<String> getChests(int map) {
        ArrayList<String> spawns = new ArrayList<String>();

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT location FROM server_minigame_sg_map_chests WHERE map = ?;");
            ps.setInt(1, map);
            rs = ps.executeQuery();

            while (rs.next()) {
                spawns.add(rs.getString("location"));
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get spawns for a map from the database.");
            closeConnection(connection);
            return null;
        }
        closeConnection(connection);
        return spawns;
    }

    public static boolean deleteChests(int map, String location) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("DELETE FROM server_minigame_sg_chests WHERE map = ? AND location = ?;");
            ps.setInt(1, map);
            ps.setString(2, location);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to delete a spawn for a map from the database.");
            closeConnection(connection);
            return false;
        }
        closeConnection(connection);
        return retVal;
    }

    public static boolean containsPlayer(int id) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT playerId FROM server_minigame_sg_leaderboard WHERE playerId = ?;");
            ps.setInt(1, id);
            rs = ps.executeQuery();

            retVal = rs.next();

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to check if a player exists in the leaderboard!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static int getPlayerId(String name) {
        int retVal = 0;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT id FROM network_users WHERE Lower(name) = Lower(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            if (rs.next()) {
                retVal = rs.getInt("id");
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get a players id!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static boolean addPlayer(int id) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO server_minigame_sg_leaderboard (playerId, wins, totalGames," +
                    " mostKills, totalKills, totalDeaths, totalPoints, totalChests, totalLife)" +
                    " VALUES (?, 0, 0, 0, 0, 0, 100, 0, 0);");
            ps.setInt(1, id);
            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to add a player to the leaderboard!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static String getRank(String name) {
        String retVal = "";

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareCall("SELECT rank FROM network_users WHERE LOWER(name) = LOWER(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                retVal = rs.getString(1);
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get a users rank from the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static boolean updatePlayerWins(int id) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE server_minigame_sg_leaderboard SET wins = wins + 1 WHERE playerId = ?;");
            ps.setInt(1, id);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players wins.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean updatePlayerTotalGames(int id) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE server_minigame_sg_leaderboard SET totalGames = totalGames + 1 WHERE playerId = ?;");
            ps.setInt(1, id);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players total games.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean addTickets(int id, int tickets) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE network_users SET tickets = tickets + ? WHERE id = ?;");
            ps.setInt(1, tickets);
            ps.setInt(2, id);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players tickets.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean addPoints(int id, int points) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE server_minigame_sg_leaderboard SET totalPoints = totalPoints + ? WHERE playerId = ?;");
            ps.setInt(1, points);
            ps.setInt(2, id);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players points.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean updateTotalKills(int id) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE server_minigame_sg_leaderboard SET totalKills = totalKills + 1 WHERE playerId = ?;");
            ps.setInt(1, id);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players kills.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static boolean updateTotalDeaths(int id) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE server_minigame_sg_leaderboard SET totalDeaths = totalDeaths + 1 WHERE playerId = ?;");
            ps.setInt(1, id);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to update current players deaths.");
            closeConnection(connection);
            return false;
        }

        return retVal;
    }

    public static int getPlayerPoints(int id) {
        int retVal = 0;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT totalPoints FROM server_minigame_sg_leaderboard WHERE playerId = ?;");
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                retVal = rs.getInt("totalPoints");
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Failed to get a players total points");
            closeConnection(connection);
        }

        return retVal;
    }

    public static ArrayList<SGItem> getItemsFromDB() {
        ArrayList<SGItem> availItemList = new ArrayList<SGItem>();

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT * FROM network_minigame_shop_items WHERE game = 'sg';");
            rs = ps.executeQuery();

            while (rs.next() != false) {
                availItemList.add(new SGItem(rs.getInt(1), rs.getString(5), rs.getString(6), rs.getString(7), Material.getMaterial(rs.getInt(2))));
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Unable to fetch SG items from database!");
            closeConnection(connection);
        }

        return availItemList;
    }

    public static int getIdForPlayer(String name) {
        int id = -1;
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareCall("SELECT id FROM network_users WHERE LOWER(name) = LOWER(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                id = rs.getInt(1);
            }
            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SsundeeSG.getPlugin().getLogger().info("Unable to get player ID");
            closeConnection(connection);
        }

        return id;
    }

    public static ArrayList<SGItem> getUserItemsFromDatabase(int playerID) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SGItem> items = new ArrayList<SGItem>();

        try {
            ps = connection.prepareCall("SELECT shopItemId from network_user_minigame_shop_items WHERE playerId = ?");
            ps.setInt(1, playerID);
            rs = ps.executeQuery();
            List<SGItem> itemsToChoose = getItemsFromDB();
            while (rs.next() != false) {
                for (SGItem i : itemsToChoose) {
                    if (i.id == rs.getInt(1)) {
                        items.add(i);
                    }
                }
            }
        } catch (SQLException ex) {
            SsundeeSG.getPlugin().getLogger().info("Unable to load player items over the DB!");
            closeConnection(connection);
        }
        closeConnection(connection);
        return items;
    }

    public static void removeItemForPlayer(int playerId, int itemId, int currentQuantity) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        if (currentQuantity > 1) {
            try {
                ps = connection.prepareStatement("UPDATE network_user_minigame_shop_items SET itemQuantity = ? WHERE playerId = ? AND shopItemId = ?;");
                ps.setInt(1, currentQuantity - 1);
                ps.setInt(2, playerId);
                ps.setInt(3, itemId);
                ps.executeUpdate();

                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
                SsundeeSG.getPlugin().getLogger().info("Unable to remove item from db!");
                closeConnection(connection);
            }
        } else {
            try {
                ps = connection.prepareStatement("DELETE FROM network_user_minigame_shop_items WHERE playerId = ? AND shopItemId = ?;");
                ps.setInt(1, playerId);
                ps.setInt(2, itemId);
                ps.executeUpdate();

                ps.close();
            } catch (SQLException e) {
                SsundeeSG.getPlugin().getLogger().info("Unable to remove item from db!");
                e.printStackTrace();
                closeConnection(connection);
            }
        }

        closeConnection(connection);
    }

    public static int getItemQuantityForItem(int itemId, int playerId) {
        int quantity = 0;
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareCall("SELECT itemQuantity FROM network_user_minigame_shop_items WHERE playerId = ? AND shopItemId = ?;");
            ps.setInt(1, playerId);
            ps.setInt(2, itemId);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                quantity = rs.getInt(1);
            }
            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            SsundeeSG.getPlugin().getLogger().info("Unable to get item quanitity!");
            closeConnection(connection);
        }

        return quantity;
    }

}
