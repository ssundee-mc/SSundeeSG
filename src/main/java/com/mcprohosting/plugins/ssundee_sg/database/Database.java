package com.mcprohosting.plugins.ssundee_sg.database;

import com.gmail.favorlock.bonesqlib.MySQL;
import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;

import java.sql.SQLException;

public class Database {

    public static MySQL mysql;
    private String host;
    private int port;
    private String database;
    private String username;
    private String password;

    public Database() {
        init();
    }

    public static boolean isDBSchemaValid() {
        boolean retVal = false;
        retVal = (mysql.isTable("server_minigame_sg_status") && mysql.isTable("server_minigame_sg_maps") && mysql.isTable("server_minigame_sg_map_spawns") && mysql.isTable("server_minigame_sg_map_chests"));
        return retVal;
    }

    public static boolean setupTables() {
        boolean retVal = false;

        try {
            if (mysql.isTable("server_minigame_sg_status") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_status` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `currentPlayers` INTEGER NOT NULL," +
                        "  `maxPlayers` INTEGER NOT NULL," +
                        "  `status` varchar(16)," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
            }
            if (mysql.isTable("server_minigame_sg_maps") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_maps` (" +
                        "  `id` int(11) NOT NULL AUTO_INCREMENT," +
                        "  `name` varchar(32) NOT NULL," +
                        "  `center` longtext NOT NULL," +
                        "  PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
            }
            if (mysql.isTable("server_minigame_sg_dm_maps") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_dm_maps` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `name` varchar(32) NOT NULL," +
                        "  `center` longtext NOT NULL," +
                        "  `radius` INTEGER NOT NULL," +
                        "  PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
            }
            if (mysql.isTable("server_minigame_sg_map_chests") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_map_chests` (\n" +
                        "    `map` int(11) NOT NULL,\n" +
                        "    `location` longtext NOT NULL,\n" +
                        "    KEY `map` (`map`),\n" +
                        "    CONSTRAINT `server_minigame_sg_map_chests_ibfk_1` \n" +
                        "        FOREIGN KEY (`map`) \n" +
                        "        REFERENCES `server_minigame_sg_maps` (`id`)\n" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            }
            if (mysql.isTable("server_minigame_sg_map_spawns") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_map_spawns` (" +
                        "  `map` int(11) NOT NULL," +
                        "  `location` longtext NOT NULL," +
                        "  KEY `map` (`map`)," +
                        "  CONSTRAINT `server_minigame_sg_map_spawns_ibfk_1`" +
                        "     FOREIGN KEY (`map`)" +
                        "     REFERENCES `server_minigame_sg_maps` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            }
            if (mysql.isTable("server_minigame_sg_leaderboard") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_leaderboard` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `wins` INTEGER NOT NULL," +
                        "  `totalGames` INTEGER NOT NULL," +
                        "  `mostKills` INTEGER NOT NULL," +
                        "  `totalKills` INTEGER NOT NULL," +
                        "  `totalDeaths` INTEGER NOT NULL," +
                        "  `totalPoints` INTEGER NOT NULL," +
                        "  `totalChests` INTEGER NOT NULL," +
                        "  `totalLife` INTEGER NOT NULL," +
                        "  UNIQUE KEY (`playerId`)," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            retVal = true;
        } catch (SQLException e) {
            try {
                if (mysql.isTable("server_minigame_sg_status") == true) {
                    mysql.query("DROP TABLE server_minigame_sg_status;");
                }
                if (mysql.isTable("server_minigame_sg_maps") == true) {
                    mysql.query("DROP TABLE server_minigame_sg_maps;");
                }
                if (mysql.isTable("server_minigame_sg_dm_maps") == true) {
                    mysql.query("DROP TABLE server_minigame_sg_dm_maps;");
                }
                if (mysql.isTable("server_minigame_sg_map_spawns") == true) {
                    mysql.query("DROP TABLE server_minigame_sg_map_spawns;");
                }
                if (mysql.isTable("server_minigame_sg_map_chests") == true) {
                    mysql.query("DROP TABLE server_minigame_sg_map_chests;");
                }
                if (mysql.isTable("server_minigame_sg_leaderboard") == true) {
                    mysql.query("DROP TABLE server_minigame_sg_leaderboard;");
                }
            } catch (SQLException ex) {
                SsundeeSG.getPlugin().getLogger().severe("Error dropping tables!");
            }
            e.printStackTrace();
        }

        return retVal;
    }

    private void init() {
        loadConfig();
        mysql = new MySQL(SsundeeSG.getPlugin().getLogger(),
                "Sssundee-SG",
                host,
                port,
                database,
                username,
                password);
        mysql.open();
    }

    private void loadConfig() {
        host = SsundeeSG.grabConfig().database_host;
        port = SsundeeSG.grabConfig().database_port;
        database = SsundeeSG.grabConfig().database_database;
        username = SsundeeSG.grabConfig().database_username;
        password = SsundeeSG.grabConfig().database_password;
    }

}
