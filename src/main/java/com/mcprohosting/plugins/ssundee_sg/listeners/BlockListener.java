package com.mcprohosting.plugins.ssundee_sg.listeners;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;

public class BlockListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getPlayer().isOp() && SsundeeSG.getPlugin().getGame().getStatus() != GameState.INPROGRESS) {
            return;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        if (event.getBlock().getType() != Material.LEAVES && event.getBlock().getType() != Material.WEB &&
                event.getBlock().getType() != Material.MELON_BLOCK && event.getBlock().getType() != Material.WHEAT &&
                event.getBlock().getType() != Material.POTATO && event.getBlock().getType() != Material.CARROT &&
                event.getBlock().getType() != Material.CROPS) {
            event.setCancelled(true);
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.INPROGRESS) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockSpread(BlockSpreadEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockDecay(LeavesDecayEvent event) {
        event.setCancelled(true);
    }

}
