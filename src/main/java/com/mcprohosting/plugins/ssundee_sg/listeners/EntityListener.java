package com.mcprohosting.plugins.ssundee_sg.listeners;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.ItemStack;

public class EntityListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onCombust(EntityCombustEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        if (event.getAttacker() != null) {
            if (event.getAttacker() instanceof Player) {
                Player player = (Player) event.getAttacker();
                Participant participant = Participant.getParticipant(player.getName());
                if (!participant.isAlive()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onHangingBreak(HangingBreakByEntityEvent event) {
        if (event.getRemover() != null) {
            if (event.getRemover() instanceof Player) {
                Player player = (Player) event.getRemover();
                Participant participant = Participant.getParticipant(player.getName());
                if (!participant.isAlive()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onDamage(EntityDamageEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            Participant participant = Participant.getParticipant(player.getName());
            if (!participant.isAlive()) {//don't damage spectators
                event.setCancelled(true);
            } else if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.INPROGRESS) {
                if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
                    player.setFallDistance(0);
                    player.teleport(player.getWorld().getSpawnLocation());
                    event.setCancelled(true);
                    return;
                }
                event.setCancelled(true);
                if (player.getHealth() - event.getDamage() <= 0) {
                    player.setHealth(20.0);
                    player.setFoodLevel(20);
                    for (ItemStack is : player.getInventory()) {
                        if (is != null && is.getType() != Material.AIR) {
                            player.getWorld().dropItemNaturally(player.getLocation(), is);
                        }
                    }
                    for (ItemStack is : player.getInventory().getArmorContents()) {
                        if (is != null && is.getType() != Material.AIR) {
                            player.getWorld().dropItemNaturally(player.getLocation(), is);
                        }
                    }
                    player.getInventory().clear();
                    player.getInventory().setArmorContents(null);
                    participant.setDead();
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityTarget(EntityTargetEvent event) {
        if (event.getTarget() instanceof Player) {
            Player player = (Player) event.getTarget();
            Participant participant = Participant.getParticipant(player.getName());
            if (!participant.isAlive()) {//don't target spectators
                event.setTarget(null);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            event.getEntity().remove(); //YOU NEVER CANCEL CREATURESPAWNEVENTS :O
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerHurtEntity(EntityDamageByEntityEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        // Prevent spectators from interfering with arrows.
        Entity entityDamager = event.getDamager();
        Entity entityDamaged = event.getEntity();
        if (!(entityDamaged instanceof Player)) {
            if(entityDamager instanceof Player) {
                Participant participant = Participant.getParticipant(((Player) entityDamager).getName());
                if(participant.checkIfHasEffect(SGItemAction.SKELETON_KILLER) &&  entityDamaged instanceof Skeleton) {
                    event.setDamage(9000.00);
                    return;
                } else if(participant.checkIfHasEffect(SGItemAction.DAYZ_PRO) && entityDamaged instanceof Zombie) {
                    event.setDamage(9000.00);
                    return;
                }
                return;
            } else {
                return;
            }
        }
        Participant participant = Participant.getParticipant(((Player) entityDamaged).getName());

        if (entityDamager instanceof Player) {
            Player player = (Player) entityDamager;
            Participant participant1 = Participant.getParticipant(player.getName());
            if (participant1.isAlive() == false) {//don't damage spectators
                event.setCancelled(true);
            }
        }

        /*if (entityDamager instanceof Arrow) {
            if (entityDamaged instanceof Player && ((Arrow) entityDamager).getShooter() instanceof Player) {
                Arrow arrow = (Arrow) entityDamager;

                Vector velocity = arrow.getVelocity();

                Player shooter = (Player) arrow.getShooter();
                Player damaged = (Player) entityDamaged;

                if (!participant.isAlive()) {
                    damaged.teleport(entityDamaged.getLocation().add(0, 5, 0));
                    damaged.setFlying(true);

                    Arrow newArrow = shooter.launchProjectile(Arrow.class);
                    newArrow.setShooter(shooter);
                    newArrow.setVelocity(velocity);
                    newArrow.setBounce(false);

                    event.setCancelled(true);
                    arrow.remove();
                }
            }
        }*/
    }


}
