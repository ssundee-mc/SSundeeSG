package com.mcprohosting.plugins.ssundee_sg.listeners;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.Database;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.events.PlayerSpectateEvent;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.game.regions.BlindnessRegion;
import com.mcprohosting.plugins.ssundee_sg.game.regions.Region;
import com.mcprohosting.plugins.ssundee_sg.items.SGItem;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import com.mcprohosting.plugins.ssundee_sg.utils.NumberUtils;
import com.mcprohosting.plugins.ssundee_sg.utils.bossbar.FakeDragon;
import com.mcprohosting.plugins.ssundee_sg.utils.map.Spawn;
import com.mcprohosting.plugins.ssundee_sg.utils.server.NetworkUtil;
import com.mcprohosting.plugins.ssundee_sg.utils.server.UpdateTab;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffectType;
import org.mcsg.double0negative.tabapi.TabAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class PlayerListener implements Listener {

    private HashMap<String, Region> playersInRegion = new HashMap<>();

    @EventHandler(priority = EventPriority.NORMAL)
    public void onJoin(PlayerJoinEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.LOBBY
                || SsundeeSG.getPlugin().getGame().getStatus() == GameState.VOTING
                || SsundeeSG.getPlugin().getGame().getStatus() == GameState.POSTVOTING
                || SsundeeSG.getPlugin().getGame().getStatus() == GameState.STARTING) {
            if (Bukkit.getOnlinePlayers().length <= 24) {
                event.setJoinMessage(ChatColor.YELLOW + "" + Bukkit.getOnlinePlayers().length +
                        " tributes have boarded the train!");
            }
            event.getPlayer().getInventory().clear();
            event.getPlayer().setExp(0.0f);
        } else {
            event.setJoinMessage(null);
        }

        DatabaseManager.updateStatus();
        final Player player = event.getPlayer();
        final Participant participant = Participant.getParticipant(player.getName());

        event.getPlayer().teleport(Bukkit.getWorlds().get(0).getSpawnLocation());//teleport to spawn at login

        for (Participant participant1 : Participant.getParticipants()) {//hide any spectators on login
            if (!participant1.isAlive()) {
                Player p1 = Bukkit.getPlayer(participant1.getName());
                if (p1 != null) {
                    player.hidePlayer(p1);
                }
            }
        }

        if (SsundeeSG.getPlugin().getGame().getStatus().equals(GameState.STARTING)) {
            for (Spawn spawn : SsundeeSG.getPlugin().getGame().getMap().getSpawns().values()) {
                if (participant.getSpawnId() == spawn.id) {
                    if (!spawn.isOccupied()) {
                        spawn.setOccupied(true);
                    }

                    player.teleport(spawn.getLocation(SsundeeSG.getPlugin().getGame().getMap().getMapName()));
                    return;
                }

                if (!spawn.isOccupied() && participant.getSpawnId() == -1) {
                    player.teleport(spawn.getLocation(SsundeeSG.getPlugin().getGame().getMap().getMapName()));
                    spawn.setOccupied(true);
                    participant.setSpawnId(spawn.id);
                    return;
                }
            }
            Bukkit.getScheduler().runTaskLater(SsundeeSG.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    participant.setDead();//no free spot :(
                }
            }, 10L);
        }

        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS) {
            Bukkit.getScheduler().runTaskLater(SsundeeSG.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    participant.setDead();//join while in progress
                }
            }, 10L);
        }

        TabAPI.setPriority(SsundeeSG.getPlugin(), player, 2); //Allow TabAPI to update this player
        Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new UpdateTab());
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLogin(PlayerLoginEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.GAMEOVER ||
                SsundeeSG.getPlugin().getGame().getStatus() == GameState.RESTARTING) {
            NetworkUtil.sendToHub(event.getPlayer());
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "You cannot join the server while it is shutting down.");
            return;
        }

        Participant participant = Participant.getParticipant(event.getPlayer().getName());

        if (participant == null) {
            participant = new Participant(event.getPlayer().getName());
            Participant.addParticipant(participant);
        }

        if (Bukkit.getOnlinePlayers().length >= Bukkit.getMaxPlayers()) {
            if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.LOBBY ||
                    SsundeeSG.getPlugin().getGame().getStatus() == GameState.VOTING ||
                    SsundeeSG.getPlugin().getGame().getStatus() == GameState.POSTVOTING) {
                ArrayList<String> kickablePlayers = Participant.findKickablePlayers(participant.getRank());

                Player playerToKick = null;
                for (String name : kickablePlayers) {
                    Player player = Bukkit.getPlayer(name);

                    if (playerToKick == null) {
                        playerToKick = player;
                    } else {
                        if (Participant.getParticipant(player.getName()).getTimeJoined() >
                                Participant.getParticipant(playerToKick.getName()).getTimeJoined()) {
                            playerToKick = player;
                        }
                    }
                }

                if (playerToKick != null) {
                    playerToKick.sendMessage(ChatColor.RED +
                            "A player with higher join priority has taken your slot. :(");
                    NetworkUtil.sendToHub(playerToKick);
                    event.allow();
                } else {
                    event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
                            "We were unable to make room for you in this server.");
                    return;
                }
            } else if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.STARTING ||
                    SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS) {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
                        "This server is in progress and has no available spectator slots.");
                return;
            }
        }

        if (!DatabaseManager.containsPlayer(participant.getPlayerId())) {
            DatabaseManager.addPlayer(participant.getPlayerId());
        }

        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS) {
            participant.setDead();//player is joining with game in progress to set dead to spectate
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Bukkit.getScheduler().runTaskLater(SsundeeSG.getPlugin(), new Runnable() {
            @Override
            public void run() {
                DatabaseManager.updateStatus();
            }
        }, 10L);
        event.getPlayer().getInventory().clear();
        event.getPlayer().getInventory().setArmorContents(null);
        FakeDragon.dragonplayers.remove(event.getPlayer().getName());
        Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new UpdateTab());

        if (Participant.getParticipant(event.getPlayer().getName()).isAlive() &&
                SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS) {
            NumberUtils.removePointsFromPlayer(Participant.getParticipant(event.getPlayer().getName()).getPlayerId());
            DatabaseManager.updateTotalDeaths(Participant.getParticipant(event.getPlayer().getName()).getPlayerId());
        }

        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.STARTING) {
            Participant p = Participant.getParticipant(event.getPlayer().getName());
            for (Spawn spawn : SsundeeSG.getPlugin().getGame().getMap().getSpawns().values()) {
                if(spawn.getId() == p.getSpawnId()) {
                    if(spawn.isOccupied()) {
                        spawn.setOccupied(false);
                    }
                }

            }
        }

        Participant.removeParticipant(event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemPickup(PlayerPickupItemEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        Player player = event.getPlayer();
        Participant participant = Participant.getParticipant(player.getName());
        if (!participant.isAlive()) {//don't allow spectators to pickup items
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemDrop(PlayerDropItemEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        Player player = event.getPlayer();
        Participant participant = Participant.getParticipant(player.getName());
        if (!participant.isAlive()) {//don't allow spectators to drop items
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(PlayerDeathEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS) {
            Player player = event.getEntity();
            Participant participant = Participant.getParticipant(player.getName());
            if (participant.isAlive()) {
                participant.setPlace();
                participant.setDead();

                // Give the killer points for killing the player.
                Player killer = player.getKiller();
                if (killer != null) {
                    if (killer.getName().equals(player.getName()) == false) {
                        DatabaseManager.addPoints(Participant.getParticipant(killer.getName()).getPlayerId(), 1);
                        DatabaseManager.addTickets(Participant.getParticipant(killer.getName()).getPlayerId(), 2);
                        DatabaseManager.updateTotalKills(Participant.getParticipant(killer.getName()).getPlayerId());
                        Participant.getParticipant(killer.getName())
                                .setCurrentKills(Participant.getParticipant(killer.getName()).getCurrentKills() + 1);
                        killer.sendMessage(ChatColor.GREEN + "You have been awarded 1 point and 2 tickets for killing " +
                                player.getName());
                        for (Participant p : Participant.getAliveParticipants()) {
                            Player target = Bukkit.getPlayer(p.getName());
                            Location tenUp = target.getLocation();
                            tenUp.setY(tenUp.getY() + 10);
                            target.playSound(tenUp, Sound.FIREWORK_LARGE_BLAST, 2, 1);
                        }
                    }
                }

                NumberUtils.removePointsFromPlayer(participant.getPlayerId());
                DatabaseManager.updateTotalDeaths(participant.getPlayerId());
            }
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new UpdateTab());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onSpectate(PlayerSpectateEvent event) {
        final Player player = event.getPlayer();

        if (player == null) {
            return;
        }

        for (Player p1 : Bukkit.getOnlinePlayers()) {
            p1.hidePlayer(player);
        }

        if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
            player.setPlayerWeather(WeatherType.CLEAR);
        }

        if (!Participant.getAliveParticipants().isEmpty()) {
            Random random = new Random(System.currentTimeMillis());
            Player p1 = Bukkit.getPlayer(Participant.getAliveParticipants()
                    .get(random.nextInt(Participant.getAliveParticipants().size())).getName());
            if (p1 != null) {
                player.teleport(p1);
            }
        }

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().height = 0;
        player.setAllowFlight(true);
        player.getActivePotionEffects().clear();
        Bukkit.getScheduler().runTaskLater(SsundeeSG.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // Add Compass
                ItemStack spectateMenu = new ItemStack(Material.COMPASS);
                ItemMeta spectatMenuMeta = spectateMenu.getItemMeta();
                spectatMenuMeta.setDisplayName("Spectate Menu");
                spectateMenu.setItemMeta(spectatMenuMeta);
                player.getInventory().setItem(0, spectateMenu);

                // Add Watch
                ItemStack returnToHub = new ItemStack(Material.WATCH);
                ItemMeta returnToHubMeta = returnToHub.getItemMeta();
                returnToHubMeta.setDisplayName("Return To Hub");
                returnToHub.setItemMeta(returnToHubMeta);
                player.getInventory().setItem(8, returnToHub);
            }
        }, 10L);
        player.sendMessage(ChatColor.GOLD + "You are now spectating.");
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
    public void onInteract(PlayerInteractEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        Player player = event.getPlayer();
        Participant participant = Participant.getParticipant(player.getName());
        if (!participant.isAlive()) {//don't allow spectators to interact
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK
                    || event.getAction() == Action.RIGHT_CLICK_AIR
                    || event.getAction() == Action.LEFT_CLICK_BLOCK
                    || event.getAction() == Action.LEFT_CLICK_AIR) {
                ItemStack is = player.getItemInHand();
                if (is == null) {
                    return;
                }
                if (is.getType() == Material.COMPASS) {
                    Inventory inv = SsundeeSG.getPlugin().getServer().createInventory(player, 27, "Alive Players");
                    for (Participant participant1 : Participant.getAliveParticipants()) {
                        ItemStack playerHead = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                        SkullMeta sm = (SkullMeta) playerHead.getItemMeta();
                        sm.setOwner(participant1.getName());

                        final String health = "Health: " + Bukkit.getPlayer(participant1.getName()).getHealth();
                        final String hunger = "Hunger: " + Bukkit.getPlayer(participant1.getName()).getFoodLevel();
                        final String kills = "Kills: " + participant1.getCurrentKills();
                        ArrayList<String> headLore = new ArrayList<String>() {{
                            add(health);
                            add(hunger);
                            add(kills);
                        }};
                        sm.setLore(headLore);
                        playerHead.setItemMeta(sm);
                        inv.addItem(playerHead);
                    }
                    player.openInventory(inv);
                }
                if (is.getType() == Material.WATCH) {
                    NetworkUtil.sendToHub(player);
                }
            }
            event.setCancelled(true);
        } else {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK
                    || event.getAction() == Action.RIGHT_CLICK_AIR
                    || event.getAction() == Action.LEFT_CLICK_BLOCK
                    || event.getAction() == Action.LEFT_CLICK_AIR) {
                ItemStack is = player.getItemInHand();
                if(is.getType() == Material.PAPER) {
                    Region playerRegion = null;
                    for(Region r : Region.getRegions().keySet()) {
                        if(r.inRegion(event.getPlayer().getLocation())) {
                            playerRegion = r;
                        }
                    }
                    if (playerRegion == null) {
                        event.getPlayer().sendMessage(ChatColor.RED+"You are not in a region that has a debuff!");
                        return;
                    }
                    SGItemAction regionAction = SGItemAction.getActionForRegion(playerRegion);
                    if (regionAction == SGItemAction.NONE) {
                        event.getPlayer().sendMessage(ChatColor.RED+"This region does not have a debuff.");
                        return;
                    }
                    if(participant.checkIfHasEffect(regionAction)) {
                        event.getPlayer().sendMessage(ChatColor.RED+"You already have the buff for this region!");
                        return;
                    }
                    SGItem playerItem = participant.getOwnedItemForEffect(regionAction);
                    if(playerItem == null) {
                        event.getPlayer().sendMessage(ChatColor.RED+"You do not own the buff item for this region!");
                        event.getPlayer().sendMessage(ChatColor.RED+"You can purchase these buffs from the SG item shop.");
                        return;
                    }
                    // Woo! Use the item!
                    participant.addCurrentItemEffect(regionAction);
                    event.getPlayer().sendMessage(ChatColor.GREEN+"You've used one of your "+playerItem.name+"(s)!");
                    DatabaseManager.removeItemForPlayer(participant.getPlayerId(), playerItem.id, DatabaseManager.getItemQuantityForItem(playerItem.id, participant.getPlayerId()));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        if (event.getSlotType() == InventoryType.SlotType.CONTAINER) {
            Player player = (Player) event.getWhoClicked();
            Participant participant = Participant.getParticipant(player.getName());
            if (!participant.isAlive()) {//don't allow spectators to move inv
                if (event.getCurrentItem() != null) {
                    ItemStack is = event.getCurrentItem();
                    if (is.getType() == Material.SKULL_ITEM) {
                        SkullMeta sm = (SkullMeta) is.getItemMeta();
                        Player p1 = Bukkit.getPlayer(sm.getOwner());
                        if (p1 != null) {
                            player.teleport(p1);
                            player.closeInventory();
                        }
                    }
                }
                event.setCancelled(true);
            } else {
                ItemStack is = event.getCurrentItem();
                if (is.getType() == Material.PAPER) { // Don't let players move shop item activator
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            return;
        }
        Player player = event.getPlayer();
        Participant participant = Participant.getParticipant(player.getName());
        if (!participant.isAlive()) {
            ArrayList<Participant> participants = Participant.getAliveParticipants();
            if (!participants.isEmpty()) {
                Random random = new Random(System.currentTimeMillis());
                Participant participant1 = participants.get(random.nextInt(participants.size()));
                Player p1 = Bukkit.getPlayer(participant1.getName());
                if (p1 != null) {
                    event.setRespawnLocation(p1.getLocation());
                    return;
                }
            }
            event.setRespawnLocation(player.getWorld().getSpawnLocation());
        } else if (participant.isAlive() && (SsundeeSG.getPlugin().getGame().getStatus() == GameState.STARTING ||
                SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS)) {
            for (Spawn spawn : SsundeeSG.getPlugin().getGame().getMap().getSpawns().values()) {
                if (participant.getSpawnId() == spawn.getId()) {
                    if (!spawn.isOccupied()) {
                        spawn.setOccupied(true);
                        return;
                    }

                    event.setRespawnLocation(spawn.getLocation(SsundeeSG.getPlugin().getGame().getMap().getMapName()));
                    return;
                }

                if (!spawn.isOccupied() && participant.getSpawnId() == -1) {
                    event.setRespawnLocation(spawn.getLocation(SsundeeSG.getPlugin().getGame().getMap().getMapName()));
                    spawn.setOccupied(true);
                    return;
                }
            }
            participant.setDead();
            ArrayList<Participant> participants = Participant.getAliveParticipants();
            if (!participants.isEmpty()) {
                Random random = new Random(System.currentTimeMillis());
                Participant participant1 = participants.get(random.nextInt(participants.size()));
                Player p1 = Bukkit.getPlayer(participant1.getName());
                if (p1 != null) {
                    event.setRespawnLocation(p1.getLocation());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onFoodChange(FoodLevelChangeEvent event) {
        Player player = (Player) event.getEntity();
        Participant participant = Participant.getParticipant(player.getName());
        if (!participant.isAlive()) {
            event.setCancelled(true);
            return;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.INPROGRESS) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Participant participant = Participant.getParticipant(event.getPlayer().getName());
        if (participant.isAlive() == false) {
            event.setCancelled(true);
            String message = "[S] " + event.getPlayer().getName() + ": " + event.getMessage();
            for (Player player : Bukkit.getOnlinePlayers()) {
                Participant other = Participant.getParticipant(player.getName());
                if (other.isAlive() == false) {
                    player.sendMessage(message);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getPlayer() == null) {
            return;
        }

        Player player = event.getPlayer();
        Participant participant = Participant.getParticipant(player.getName());
        Location to = event.getTo();
        Location from = event.getFrom();
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.STARTING) {
            if (to.getBlockX() != from.getBlockX()
                    || to.getBlockZ() != from.getBlockZ()) {

                if (participant.getSpawnId() != -1) {
                    player.teleport(SsundeeSG.getPlugin().getGame().getMap().getSpawns().get(participant.getSpawnId())
                            .getLocation(SsundeeSG.getPlugin().getGame().getMap().getMapName()).add(0.5, 0.5, 0.5));
                }

                return;
            }
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS
                || SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            Region toRegion = null;
            for (Region region : Region.getRegions().keySet()) {
                if (region.inRegion(to)) {
                    toRegion = region;
                    region.regionWeatherEffect(player);
                    if (playersInRegion.get(player.getName()) != region
                            && SsundeeSG.getPlugin().getGame().getSeconds() < 900) {
                        player.sendMessage(ChatColor.RED + "You entered Sector " + Region.getRegions().get(region));
                        player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 10.0F, 1.0F);
                        playersInRegion.put(player.getName(), region);
                    }
                    if (!(region instanceof BlindnessRegion)) {
                        if (player.hasPotionEffect(PotionEffectType.BLINDNESS)) {
                            player.removePotionEffect(PotionEffectType.BLINDNESS);
                        }
                    }
                    break;
                }
            }
            if (toRegion == null) {
                if (player.hasPotionEffect(PotionEffectType.BLINDNESS)) {
                    player.removePotionEffect(PotionEffectType.BLINDNESS);
                }
                if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
                    player.setPlayerWeather(WeatherType.CLEAR);
                }
            }
        }
    }

}
