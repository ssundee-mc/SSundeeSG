package com.mcprohosting.plugins.ssundee_sg.utils;

import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.items.SGItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class ItemUtils {

    public static ItemStack generatePaperWithMetaForPlayer(Player p) {
        ItemStack paper = new ItemStack(Material.PAPER);
        Participant participant = Participant.getParticipant(p.getName());
        participant.setOwnedItems(DatabaseManager.getUserItemsFromDatabase(participant.getPlayerId()));
        ItemMeta paperMeta = paper.getItemMeta();
        paperMeta.setDisplayName("Your SG Buffs");
        ArrayList<String> paperLore = new ArrayList<String>();
        for(SGItem i : participant.getOwnedItems()) {
            paperLore.add(i.name+" - x"+DatabaseManager.getItemQuantityForItem(i.id, participant.getPlayerId()));
        }
        paperMeta.setLore(paperLore);
        paper.setItemMeta(paperMeta);
        return paper;
    }

}
