package com.mcprohosting.plugins.ssundee_sg.utils.server;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class MoveServer implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            NetworkUtil.sendToHub(player);
        }
    }
}

