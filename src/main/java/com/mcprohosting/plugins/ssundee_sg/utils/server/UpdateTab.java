package com.mcprohosting.plugins.ssundee_sg.utils.server;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.game.logic.Game;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.double0negative.tabapi.TabAPI;

import java.util.ArrayList;
import java.util.Collection;

public class UpdateTab implements Runnable {
    private class TabItem {
        public String data;
        public int xPos;
        public int yPos;

        public TabItem(String data, int xPos, int yPos) {
            this.data = data;
            this.xPos = xPos;
            this.yPos = yPos;
        }
    }

    @Override
    public void run() {
        Collection<Participant> participants = Participant.getParticipants();
        ArrayList<TabItem> items = new ArrayList<TabItem>();
        int tributeCounter = 2;
        int xposTributes = 0;
        int specCounter = 2;
        int deadCounter = 0;
        for (Participant p : participants) {
            if (Bukkit.getPlayer(p.getName()) == null)
                continue;
            if (!Bukkit.getPlayer(p.getName()).isOnline())
                continue;
            if (p.isTribute()) {
                int xPos;
                if (tributeCounter > 18) {
                    xPos = 1;
                    xposTributes++;
                } else {
                    xPos = 0;
                }
                if (p.isAlive()) {
                    items.add(new TabItem(p.getName() + TabAPI.nextNull(), xPos, tributeCounter));
                } else {
                    items.add(new TabItem(ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH.toString() + p.getName()
                            + TabAPI.nextNull(), xPos, tributeCounter));
                    deadCounter++;
                }
                tributeCounter++;
            } else {
                if (specCounter >= 19) {
                    continue;
                }
                items.add(new TabItem(p.getName() + TabAPI.nextNull(), 2, specCounter));
                specCounter++;
            }
        }

        items.add(new TabItem("Tributes - " + ((tributeCounter - 2) - deadCounter), 0, 0));
        items.add(new TabItem("Spectators - " + (specCounter - 2), 2, 0));
        items.add(new TabItem("-------------" + TabAPI.nextNull(), 0, 1));
        items.add(new TabItem("-------------" + TabAPI.nextNull(), 2, 1));

        try {
            for (int i = tributeCounter; i <= (TabAPI.getVertSize()) - tributeCounter; i++) {
                items.add(new TabItem("" + TabAPI.nextNull(), 0, i));
            }

            for (int i = xposTributes; i <= (TabAPI.getVertSize() - 1) - xposTributes; i++) {
                items.add(new TabItem("" + TabAPI.nextNull(), 1, i));
            }

            for (int i = specCounter; i <= (TabAPI.getVertSize()) - specCounter; i++) {
                items.add(new TabItem("" + TabAPI.nextNull(), 2, i));
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            SsundeeSG.getPlugin().getLogger().warning("Out of bounds when writing whitespace in the tab menu!");
        }

        for (Player p : SsundeeSG.getPlugin().getServer().getOnlinePlayers()) {
            TabAPI.clearTab(p);
            for (TabItem t : items) {
                TabAPI.setTabString(SsundeeSG.getPlugin(), p, t.yPos, t.xPos, t.data);
            }
            TabAPI.updatePlayer(p);
        }
    }
}
