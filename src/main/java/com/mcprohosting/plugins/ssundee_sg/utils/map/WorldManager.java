package com.mcprohosting.plugins.ssundee_sg.utils.map;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class WorldManager {

    public static boolean importWorld(String world, boolean overWrite) {
        File directory = new File(SsundeeSG.getPlugin().getDataFolder(), "worlds");
        File target = new File(SsundeeSG.getPlugin().getServer().getWorldContainer(), world);
        File source = new File(directory, world);

        boolean errors = false;
        if (target.exists() && target.isDirectory() && !overWrite) {
            SsundeeSG.getPlugin().getLogger().info("Not overwriting map. Skipping " + world);
            return true;
        }
        if (target.exists() && target.isDirectory() && !delete(target)) {
            SsundeeSG.getPlugin().getLogger().info("Failed to reset world \"" + source.getName() + "\" - could not delete old world folder.");
            errors = true;
        } else {
            try {
                copyDir(source, target);
            } catch (IOException e) {
                e.printStackTrace();
                SsundeeSG.getPlugin().getLogger().info("Failed to reset world \"" + source.getName() + "\" - could not import the world from backup.");
                errors = true;
            }
        }
        SsundeeSG.getPlugin().getLogger().info("Import of world \"" + source.getName() + "\" " + (errors ? "failed!" : "succeeded!"));
        return errors;
    }

    public static boolean saveWorld(String world) {
        File source = new File(SsundeeSG.getPlugin().getServer().getWorldContainer(), world);
        File worlds = new File(SsundeeSG.getPlugin().getDataFolder(), "worlds");
        File target = new File(worlds, world);

        boolean errors = false;
        if (source.exists() == false || source.isDirectory() == false) {
            SsundeeSG.getPlugin().getLogger().info("Source file is not a directory or does not exist.");
            errors = true;

            return errors;
        }

        if (target.exists() && target.isDirectory()) {
            try {
                copyDir(source, target);
            } catch (IOException e) {
                SsundeeSG.getPlugin().getLogger().info("Failed to save world: " + world + ".");
                e.printStackTrace();
                errors = true;
            }
        }

        SsundeeSG.getPlugin().getLogger().info("Saving world: " + world + " " + (errors ? "failed" : "succeeded!"));
        return errors;
    }

    public static ArrayList<String> getAvailableMaps() {
        ArrayList<String> maps = new ArrayList<String>();
        File folder = new File(SsundeeSG.getPlugin().getDataFolder(), "worlds");

        if (!folder.isDirectory()) {
            folder.delete();
        }

        if (!folder.exists()) {
            folder.mkdir();
        }

        String[] names = folder.list();

        for (String name : names) {
            File file = new File(folder, name);
            if (file.isDirectory()) {
                maps.add(name);
            }
        }

        return maps;
    }

    private static boolean delete(File file) {
        if (file.isDirectory()) {
            for (File subfile : file.listFiles()) {
                if (!delete(subfile)) {
                    return false;
                }
            }
        }
        if (!file.delete()) {
            return false;
        }
        return true;
    }

    private static void copyDir(File source, File target) throws IOException {
        if (source.isDirectory()) {
            if (!target.exists()) {
                target.mkdir();
            }
            String[] files = source.list();
            for (String file : files) {
                File srcFile = new File(source, file);
                File destFile = new File(target, file);
                copyDir(srcFile, destFile);
            }
        } else {
            InputStream in = new FileInputStream(source);
            OutputStream out = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.close();
        }
    }

    public static GameMap getDeathmatchMap() {
        ArrayList<GameMap> maps = DatabaseManager.getDeathmatchMaps();

        if (maps.size() == 0) {
            return null;
        }

        for (GameMap map : maps) {
            if (getAvailableMaps().contains(map.getMapName()) == false) {
                return map;
            }
        }

        Random random = new Random();
        return maps.get(random.nextInt(maps.size()));
    }

}
