package com.mcprohosting.plugins.ssundee_sg.utils.map;

import com.mcprohosting.plugins.ssundee_sg.config.ConfigObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Spawn extends ConfigObject {


    private boolean occupied;

    public Spawn() {
    }

    public Spawn(int id, Location location) {
        this.id = id;
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
        this.occupied = false;
    }

    public int id = 0;
    public int x = 0;
    public int y = 0;
    public int z = 0;
    public double pitch = 0.0;
    public double yaw = 0.0;

    public Location getLocation(String world) {
        return new Location(Bukkit.getWorld(world), x, y, z, (float) yaw, (float) pitch);
    }

    public boolean isOccupied() { return this.occupied; }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public int getId() {
        return id;
    }

}
