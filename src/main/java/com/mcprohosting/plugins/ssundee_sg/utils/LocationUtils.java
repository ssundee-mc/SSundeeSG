package com.mcprohosting.plugins.ssundee_sg.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationUtils {

    public static Location StringToLocation(String location) {
        String[] info = location.split(",");
        return new Location(Bukkit.getWorld(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2]), Integer.parseInt(info[3]), Float.parseFloat(info[4]), Float.parseFloat(info[5]));
    }

    public static String LocationToString(Location loc) {
        return loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }


}
