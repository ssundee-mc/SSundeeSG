package com.mcprohosting.plugins.ssundee_sg.utils.map;

import org.bukkit.Location;

import java.util.HashMap;

public class GameMap {

    private final String mapName;
    private final int id;
    private final Location mapCenter;
    private final int radius;
    private HashMap<Integer, Spawn> spawns = new HashMap<>();

    public GameMap(int id, String mapName, Location mapCenter) {
        this.id = id;
        this.mapCenter = mapCenter;
        this.mapName = mapName;
        this.radius = 0;
    }

    public GameMap(int id, String mapName, Location mapCenter, int radius) {
        this.id = id;
        this.mapCenter = mapCenter;
        this.mapName = mapName;
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public Location getMapCenter() {
        return mapCenter;
    }

    public String getMapName() {
        return mapName;
    }

    public HashMap<Integer, Spawn> getSpawns() {
        return spawns;
    }

    public String toString() {
        return mapName;
    }

    public int getRadius() {
        return radius;
    }
}
