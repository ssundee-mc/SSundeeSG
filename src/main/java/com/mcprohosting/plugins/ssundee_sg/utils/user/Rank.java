package com.mcprohosting.plugins.ssundee_sg.utils.user;

import java.util.ArrayList;

public enum Rank {

    DEFAULT("DEFAULT"),
    LAPIS("LAPIS"),
    REDSTONE("REDSTONE"),
    GOLD("GOLD"),
    DIAMOND("DIAMOND"),
    YOUTUBER("YOUTUBER"),
    MOD("MOD"),
    ADMIN("ADMIN"),
    DEV("DEV"),
    OWNER("OWNER");

    private final String rank;
    private final int priority;

    Rank(String rank) {
        this.rank = rank;
        priority = assignPriority();
    }

    public String toString() {
        return this.rank;
    }

    private int assignPriority() {
        switch (this.rank) {
            case "LAPIS":
                return 1;
            case "REDSTONE":
                return 2;
            case "GOLD":
                return 3;
            case "DIAMOND":
                return 4;
            case "YOUTUBER":
                return 5;
            case "MOD":
                return 6;
            case "ADMIN":
                return 7;
            case "DEV":
                return 8;
            case "OWNER":
                return 9;
            default:
                return 0;
        }
    }

    public static ArrayList<Rank> getLowerPriorities(String rank) {
        ArrayList<Rank> list = new ArrayList<>();

        for (Rank value : Rank.values()) {
            if (value.priority < valueOf(rank).priority) {
                list.add(value);
            }
        }

        return list;
    }
}
