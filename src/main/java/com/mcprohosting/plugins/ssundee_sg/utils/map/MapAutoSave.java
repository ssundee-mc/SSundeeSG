package com.mcprohosting.plugins.ssundee_sg.utils.map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MapAutoSave implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage("Saving all maps!");
        }

        for (String map : WorldManager.getAvailableMaps()) {
            WorldManager.saveWorld(map);
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage("All maps auto saved!");
        }
    }

}
