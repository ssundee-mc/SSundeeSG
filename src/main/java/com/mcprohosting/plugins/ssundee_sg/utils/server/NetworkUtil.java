package com.mcprohosting.plugins.ssundee_sg.utils.server;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NetworkUtil {

    public static void sendToHub(Player player) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);


        try {
            out.writeUTF("Connect");
            out.writeUTF(SsundeeSG.grabConfig().settings_lobbyserver);
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.sendPluginMessage(SsundeeSG.getPlugin(), "BungeeCord", b.toByteArray());
    }
}
