package com.mcprohosting.plugins.ssundee_sg.utils.bossbar;

import com.gmail.favorlock.PacketFactory;
import com.gmail.favorlock.entity.EntityHandler;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class FakeDragon {

    public static final int MAX_HEALTH = 200;
    public static Integer ENTITY_ID = 6000;
    public static Map<String, FakeDragon> dragonplayers = new HashMap<String, FakeDragon>();
    public boolean visible;
    public int EntityID;
    public int x;
    public int y;
    public int z;
    public float health;
    public String name;

    public FakeDragon(String name, int EntityID, Location loc) {
        this(name, EntityID, (int) Math.floor(loc.getBlockX() * 32.0D), (int) Math.floor(loc.getBlockY() * 32.0D), (int) Math.floor(loc.getBlockZ() * 32.0D));
    }

    public FakeDragon(String name, int EntityID, int x, int y, int z) {
        this(name, EntityID, x, y, z, MAX_HEALTH, false);
    }

    public FakeDragon(String name, int EntityID, int x, int y, int z, float health, boolean visible) {
        this.name = name;
        this.EntityID = EntityID;
        this.x = x;
        this.y = y;
        this.z = z;
        this.health = health;
        this.visible = visible;
    }

    public static void setStatus(Player player, String text, int healthpercent) {
        FakeDragon dragon = dragonplayers.get(player.getName());

        if (dragon != null && dragon.name.equalsIgnoreCase(text) == false) {
            Object destroyPacket = PacketFactory.getEntityDestroyPacket(dragon.EntityID);
            EntityHandler.sendPacket(player, destroyPacket);
            dragonplayers.remove(player.getName());
        }

        if (dragon != null && dragon.name.equalsIgnoreCase(text) == true) {
            return;
        }

        if (!text.equals("")) {
            dragon = new FakeDragon(text, ENTITY_ID, player.getLocation().add(0, -200, 0));
            dragonplayers.put(player.getName(), dragon);
        }

        if (text.equals("")) {
            if (dragon != null) {
                Object destroyPacket = PacketFactory.getEntityDestroyPacket(dragon.EntityID);
                EntityHandler.sendPacket(player, destroyPacket);
                dragonplayers.remove(player.getName());
            }
            return;
        }

        Object mobPacket = dragon.getMobPacket(player, text);
        General.sendPacket(player, mobPacket);

        dragon.health = (healthpercent / 100f) * FakeDragon.MAX_HEALTH;
        Object metaPacket = dragon.getMetadataPacket(dragon.getWatcher(player));
        Object teleportPacket = dragon.getTeleportPacket(player.getLocation().add(0, -200, 0));
        General.sendPacket(player, metaPacket);
        General.sendPacket(player, teleportPacket);
    }

    public static void displayDragonLoadingBar(final Plugin plugin, final String text, final String completeText, final Player player, final int healthAdd, final long delay, final boolean loadUp) {
        setStatus(player, "", (loadUp ? 1 : 100));

        new BukkitRunnable() {
            int health = (loadUp ? 1 : 100);

            @Override
            public void run() {
                if ((loadUp ? health < 100 : health > 1)) {
                    setStatus(player, text, health);
                    if (loadUp) {
                        health += healthAdd;
                    } else {
                        health -= healthAdd;
                    }
                } else {
                    setStatus(player, completeText, (loadUp ? 100 : 1));
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            setStatus(player, "", (loadUp ? 100 : 1));
                        }
                    }.runTaskLater(plugin, 20);

                    this.cancel();
                }
            }
        }.runTaskTimer(plugin, delay, delay);
    }

    public Object getMobPacket(Player player, String name) {
        Object watcher = EntityHandler.getWatcher(player, false, 100, name);
        Object packet = PacketFactory.getSpawnEntityLivingPacket(ENTITY_ID,
                EntityType.ENDER_DRAGON.getTypeId(),
                player.getLocation(),
                watcher);

        return packet;
    }

    public Object getMetadataPacket(Object watcher) {
        Class<?> packet_class = General.getCraftClass("PacketPlayOutEntityMetadata");
        Object packet = null;
        try {
            packet = packet_class.newInstance();

            Field a = General.getField(packet_class, "a");
            a.setAccessible(true);
            a.set(packet, EntityID);

            Method watcher_c = General.getMethod(watcher.getClass(), "c");
            Field b = General.getField(packet_class, "b");
            b.setAccessible(true);
            b.set(packet, watcher_c.invoke(watcher));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return packet;
    }

    public Object getTeleportPacket(Location loc) {
        Class<?> packet_class = General.getCraftClass("PacketPlayOutEntityTeleport");
        Object packet = null;
        try {
            packet = packet_class.newInstance();

            Field a = General.getField(packet_class, "a");
            a.setAccessible(true);
            a.set(packet, EntityID);
            Field b = General.getField(packet_class, "b");
            b.setAccessible(true);
            b.set(packet, (int) Math.floor(loc.getX() * 32.0D));
            Field c = General.getField(packet_class, "c");
            c.setAccessible(true);
            c.set(packet, (int) Math.floor(loc.getY() * 32.0D));
            Field d = General.getField(packet_class, "d");
            d.setAccessible(true);
            d.set(packet, (int) Math.floor(loc.getZ() * 32.0D));
            Field e = General.getField(packet_class, "e");
            e.setAccessible(true);
            e.set(packet, (byte) ((int) (loc.getYaw() * 256.0F / 360.0F)));
            Field f = General.getField(packet_class, "f");
            f.setAccessible(true);
            f.set(packet, (byte) ((int) (loc.getPitch() * 256.0F / 360.0F)));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return packet;
    }

    public Object getWatcher(Player player) {
        Class<?> watcher_class = General.getCraftClass("DataWatcher");
        Object watcher = null;
        try {
            Object entity = General.getHandle(player);
            watcher = watcher_class.getConstructors()[0].newInstance(entity);

            Method a = General.getMethod(watcher_class, "a", new Class<?>[]{int.class, Object.class});
            a.setAccessible(true);

            a.invoke(watcher, 0, visible ? (byte) 0 : (byte) 0x20);
            a.invoke(watcher, 6, (Float) (float) health);
            a.invoke(watcher, 7, (Integer) (int) 0);
            a.invoke(watcher, 8, (Byte) (byte) 0);
            a.invoke(watcher, 10, (String) name);
            a.invoke(watcher, 11, (Byte) (byte) 1);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return watcher;
    }

}
