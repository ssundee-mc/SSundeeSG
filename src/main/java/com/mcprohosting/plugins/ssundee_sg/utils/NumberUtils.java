package com.mcprohosting.plugins.ssundee_sg.utils;

import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;

public class NumberUtils {

    public static int convertToInt(String value) {
        try {
            int number = Integer.parseInt(value);
            return number;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static void removePointsFromPlayer(int id) {
        int points = DatabaseManager.getPlayerPoints(id);

        if (points < 1000) {
            DatabaseManager.addPoints(id, -3);
        } else if ((points >= 1000) && (points < 2000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.01));
        } else if ((points >= 2000) && (points < 3000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.011));
        } else if ((points >= 3000) && (points < 4000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.012));
        } else if ((points >= 4000) && (points < 5000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.013));
        } else if ((points >= 5000) && (points < 6000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.014));
        } else if ((points >= 6000) && (points < 7000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.015));
        } else if ((points >= 7000) && (points < 8000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.016));
        } else if ((points >= 8000) && (points < 9000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.017));
        } else if ((points >= 9000) && (points < 10000)) {
            DatabaseManager.addPoints(id, (int) (-points * 0.018));
        } else if (points > 10000) {
            DatabaseManager.addPoints(id, (int) (-points * 0.02));
        }
    }

}
