package com.mcprohosting.plugins.ssundee_sg.utils.server;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.Random;

public class LaunchFirework implements Runnable {

    private Location destination;
    private boolean randomFirework;
    private FireworkEffect fireworkEffect = null;


    /**
     * Launches a firework with random attributes at a given Location.
     *
     * @param destination The Location to launch the firework at.
     */
    public LaunchFirework(Location destination) {
        this.destination = destination;
        this.randomFirework = true;
    }

    /**
     * Launches a firework with random attributes at a given Location.
     *
     * @param destination The Location to launch the firework at.
     * @param effect      Specific FireworkEffect you'd like to use.
     */
    public LaunchFirework(Location destination, FireworkEffect effect) {
        this.destination = destination;
        this.randomFirework = false;
        this.fireworkEffect = effect;
    }

    @Override
    public void run() {
        Random fireworkRNG = new Random(); // RNG For firework type, offsets.
        Firework fw = (Firework) destination.getWorld().spawnEntity(destination, EntityType.FIREWORK);
        FireworkMeta fwMetadata = fw.getFireworkMeta(); // Metadata container for the firework;
        if (randomFirework || fireworkEffect == null) {
            // Create firework attributes randomly
            FireworkEffect.Type fwKind = getFireworkTypeFromInt(fireworkRNG.nextInt(5));
            Color color1 = getColorFromInt(fireworkRNG.nextInt(17) + 1);
            Color color2 = getColorFromInt(fireworkRNG.nextInt(17) + 1);
            // Create the firework effect
            FireworkEffect effect = FireworkEffect.builder().flicker(fireworkRNG.nextBoolean()).withColor(color1)
                    .withFade(color2).with(fwKind).trail(fireworkRNG.nextBoolean()).build();
            // Add effect to the metadata
            fwMetadata.addEffect(effect);

        } else {
            fwMetadata.addEffect(fireworkEffect);

        }
        // Generate the power
        fwMetadata.setPower(fireworkRNG.nextInt(2) + 1);

        // Apply the data
        fw.setFireworkMeta(fwMetadata);
    }

    private FireworkEffect.Type getFireworkTypeFromInt(int type) {
        switch (type) {
            case 0:
                return FireworkEffect.Type.BALL;
            case 1:
                return FireworkEffect.Type.BALL_LARGE;
            case 2:
                return FireworkEffect.Type.BURST;
            case 3:
                return FireworkEffect.Type.CREEPER;
            case 4:
                return FireworkEffect.Type.STAR;
            default:
                return FireworkEffect.Type.BALL;
        }
    }

    private Color getColorFromInt(int color) {
        switch (color) {
            default:
                return Color.AQUA;
            case 1:
                return Color.AQUA;
            case 2:
                return Color.BLACK;
            case 3:
                return Color.BLUE;
            case 4:
                return Color.FUCHSIA;
            case 5:
                return Color.GRAY;
            case 6:
                return Color.GREEN;
            case 7:
                return Color.LIME;
            case 8:
                return Color.MAROON;
            case 9:
                return Color.NAVY;
            case 10:
                return Color.OLIVE;
            case 11:
                return Color.ORANGE;
            case 12:
                return Color.PURPLE;
            case 13:
                return Color.RED;
            case 14:
                return Color.SILVER;
            case 15:
                return Color.TEAL;
            case 16:
                return Color.WHITE;
            case 17:
                return Color.YELLOW;
        }
    }
}
