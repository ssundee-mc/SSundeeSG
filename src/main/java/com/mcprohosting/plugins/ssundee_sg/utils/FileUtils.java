package com.mcprohosting.plugins.ssundee_sg.utils;

public class FileUtils {

    public static String getExtension(String name) {
        String extension = "";

        int i = name.lastIndexOf('.');
        if (i > 0) {
            extension = name.substring(i + 1);
        }

        return extension;
    }

}
