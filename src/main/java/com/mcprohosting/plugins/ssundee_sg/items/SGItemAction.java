package com.mcprohosting.plugins.ssundee_sg.items;

import com.mcprohosting.plugins.ssundee_sg.game.regions.*;

public enum SGItemAction {
    NONE,
    NO_SLOW(SlownessRegion.class),
    NO_POISON(PoisonRegion.class),
    SAFE_STRIKE(LightningRegion.class), // Safe from lightning strike
    SKELETON_KILLER(SkeletonRegion.class), // One shot Wolves
    DAYZ_PRO(ZombieRegion.class), // One shot Zombies
    NO_BLIND(BlindnessRegion.class),
    NO_ACIDRAIN(AcidRainRegion.class), // Block Acid Rain
    SUPER_STRONG(WeaknessRegion.class);

    private Class<? extends Region> regionClass;

    private SGItemAction() {
        this.regionClass = null;
    }

    private SGItemAction(Class<? extends Region> regionClass) {
        this.regionClass = regionClass;
    }

    public Class<? extends Region> getRegionClass() {
        return this.regionClass;
    }

    public static SGItemAction getItemActionFromString(String s) {
        String lookup = s.toLowerCase();
        switch (lookup) {
            case "no_slowness":
                return NO_SLOW;
            case "no_poison":
                return NO_POISON;
            case "no_lightning":
                return SAFE_STRIKE;
            case "oneshot_skeletons":
                return SKELETON_KILLER;
            case "oneshot_zombies":
                return DAYZ_PRO;
            case "no_blindness":
                return NO_BLIND;
            case "no_acidrain":
                return NO_ACIDRAIN;
            case "no_weakness":
                return SUPER_STRONG;
            default:
                return NONE;
        }
    }


    public static SGItemAction getActionForRegion(Region r) {
        for(SGItemAction a : SGItemAction.values()) {
            if(a.getRegionClass() != null && a.getRegionClass().isInstance(r)) {
                return a;
            }
        }
        return NONE;
    }

}
