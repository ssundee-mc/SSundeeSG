package com.mcprohosting.plugins.ssundee_sg.items;


import org.bukkit.Material;

public class SGItem {
    public int id;
    public String name;
    public String description;
    public SGItemAction action;
    public Material material;

    public SGItem(int id, String name, String description, String action, Material material) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.action = SGItemAction.getItemActionFromString(action);
        this.material = material;
    }
}
