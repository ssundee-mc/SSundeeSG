package com.mcprohosting.plugins.ssundee_sg.config;

import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import org.bukkit.plugin.Plugin;

import java.io.File;

public class Config extends ConfigModel {

    public Config(Plugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), "config.yml");
        CONFIG_HEADER = "Ssundee Games Configuration File";
    }

    public int settings_serverid = 0;
    public int settings_minplayers = 10;
    public String settings_lobbyserver = "sgLobby";
    public String database_host = "localhost";
    public int database_port = 3306;
    public String database_database = "minecraft";
    public String database_username = "admin";
    public String database_password = "1234";

    public void setServerID() {
        if (settings_serverid == 0) {
            settings_serverid = DatabaseManager.getLastServerAdded() + 1;
            DatabaseManager.addServer();
        } else {
            if (DatabaseManager.checkServer() == false) {
                DatabaseManager.reAddServer();
            }
        }
    }
}
