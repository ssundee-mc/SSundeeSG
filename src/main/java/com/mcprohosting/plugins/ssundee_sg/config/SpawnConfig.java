package com.mcprohosting.plugins.ssundee_sg.config;

import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.utils.map.Spawn;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class SpawnConfig extends ConfigModel {

    private final String world;
    private int size;

    public SpawnConfig(Plugin plugin, String world) {
        this.world = world;
        CONFIG_FILE = new File(plugin.getDataFolder() + File.separator + "configs", world + ".yml");
        CONFIG_HEADER = world + " Spawn Configuration File";
    }

    public List<Spawn> spawn = new ArrayList<Spawn>();

    public String getWorld() {
        return world;
    }

}
