package com.mcprohosting.plugins.ssundee_sg.config;

import com.mcprohosting.plugins.ssundee_sg.game.items.Item;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;

public class ItemConfig extends ConfigModel {

    public ItemConfig(Plugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), "items.yml");
        CONFIG_HEADER = "Items Configuration File";
    }

    public int maxChestValue = 100;
    public int minChestValue = 100;
    public ArrayList<Item> items = new ArrayList<Item>() {{
        add(new Item(256, 0, 1, 20));
        add(new Item(261, 0, 1, 36));
        add(new Item(276, 0, 1, 90));
    }};
}
