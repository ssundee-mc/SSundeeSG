package com.mcprohosting.plugins.ssundee_sg.config;

import org.bukkit.plugin.Plugin;

import java.io.File;

public class WorldInfoConfig extends ConfigModel {

    public WorldInfoConfig(Plugin plugin, File folder) {
        CONFIG_FILE = new File(folder, "message.yml");
    }

    public String message = "";

    public String getMessage() {
        return message;
    }

}
