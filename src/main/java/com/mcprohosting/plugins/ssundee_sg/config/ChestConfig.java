package com.mcprohosting.plugins.ssundee_sg.config;

import com.mcprohosting.plugins.ssundee_sg.game.items.ItemChest;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class ChestConfig extends ConfigModel {

    private final String world;

    public ChestConfig(Plugin plugin, String world) {
        this.world = world;
        CONFIG_FILE = new File(plugin.getDataFolder() + File.separator + "configs", world + "Chests.yml");
        CONFIG_HEADER = world + " Chest Location Configuration File";
    }

    public List<Location> locations = new ArrayList<Location>();

    public void convertToItemChest() {
        for (Location location : locations) {
            ItemChest.getChests().add(new ItemChest(location));
        }
    }

    public String getWorld() {
        return world;
    }

}
