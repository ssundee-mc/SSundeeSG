package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.gmail.favorlock.effects.ParticleEffect;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class WeaknessRegion extends Region {

    public WeaknessRegion(int angle) {
        super(angle);
    }

    @Override
    public void init() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void tick() {
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (participant.isAlive()) {
                    if (inRegion(player.getLocation()) && !participant.checkIfHasEffect(SGItemAction.SUPER_STRONG)) {
                        if (!player.hasPotionEffect(PotionEffectType.WEAKNESS)) {
                            player.sendMessage(ChatColor.RED + "You were weakened.");
                        }
                        PotionEffect potionEffect = new PotionEffect(PotionEffectType.WEAKNESS, 80, 1);
                        player.addPotionEffect(potionEffect, true);

                        regionEffect(player);
                    }
                } else {
                    if (inRegion(player.getLocation())) {
                        regionEffect(player);
                    }
                }
            }
        }
    }

    private void regionEffect(Player player) {
        Random random = new Random();
        for (int val = 0; val < 60; val++) {
            double x = (double) random.nextInt(16) + random.nextDouble() - 8.0;
            double z = (double) random.nextInt(16) + random.nextDouble() - 8.0;
            Location location = player.getLocation().add(x, 0, z);
            ParticleEffect.sendPacket(player, location, ParticleEffect.MOB_SPELL);
        }
    }

    public void regionWeatherEffect(Player player) {
        if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
            player.setPlayerWeather(WeatherType.CLEAR);
        }
    }

    public void deactivate() {

    }

    public String regionName() {
        return "Weakness";
    }

}
