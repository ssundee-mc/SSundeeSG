package com.mcprohosting.plugins.ssundee_sg.game.logic;

public enum GameState {

    LOBBY("lobby"),
    VOTING("voting"),
    POSTVOTING("postvoting"),
    STARTING("starting"),
    INPROGRESS("inprogress"),
    GAMEOVER("gameover"),
    RESTARTING("restarting"),
    MAINTENANCE("maintenance");

    private final String status;

    GameState(String status) {
        this.status = status;
    }

    public String toString() {
        return this.status;
    }

}
