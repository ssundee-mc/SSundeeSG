package com.mcprohosting.plugins.ssundee_sg.game.items;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;

import java.util.ArrayList;
import java.util.Random;

public class ItemChest {

    private static ArrayList<ItemChest> chests;
    private final Location location;

    static {
        chests = new ArrayList<>();
    }

    public ItemChest(Location location) {
        this.location = location;
        generateItems();
    }

    private void generateItems() {
        if (!location.getChunk().isLoaded()) {
            location.getChunk().load();
        }
        Block block = location.getBlock();
        Chest chest = null;
        try {
            chest = (Chest) block.getState();
        } catch (Exception ex) {
            SsundeeSG.getPlugin().getLogger().warning("Error with chest at " + location);
            return;
        }
        chest.getInventory().clear();
        Random random = new Random(System.nanoTime());
        int chestValue = random.nextInt((SsundeeSG.grabItemConfig().maxChestValue - SsundeeSG.grabItemConfig().minChestValue) + 1) + SsundeeSG.grabItemConfig().minChestValue;
        while (chestValue > 0) {
            Item item = getRandomItem(random, chestValue);
            if (item == null) {
                break;
            }
            chestValue -= item.itemValue;
            int slot = random.nextInt(chest.getInventory().getSize());
            int tries = 0;
            while (chest.getInventory().getItem(slot) != null && tries < 3) {//loop until empty slot is found or until tries gets to 3
                slot = random.nextInt(chest.getInventory().getSize());
                tries += 1;
            }
            if (tries < 3) {
                chest.getInventory().setItem(slot, item.getItemStack());
            }
        }
        chest.update();
    }

    private Item getRandomItem(Random random, int itemValue) {
        ArrayList<Item> valuedItems = new ArrayList<>();
        for (Item item : SsundeeSG.grabItemConfig().items) {
            if (item.itemValue <= itemValue) {
                valuedItems.add(item);
            }
        }
        if (valuedItems.isEmpty()) {
            return null;
        }
        return valuedItems.get(random.nextInt(valuedItems.size()));
    }

    public static ArrayList<ItemChest> getChests() {
        return chests;
    }
}
