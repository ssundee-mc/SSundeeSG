package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.gmail.favorlock.effects.ParticleEffect;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class BlindnessRegion extends Region {

    public BlindnessRegion(int angle) {
        super(angle);
    }

    @Override
    public void tick() {
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    if (participant.isAlive() && !participant.checkIfHasEffect(SGItemAction.NO_BLIND)) {
                        PotionEffect potionEffect = new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1);
                        player.addPotionEffect(potionEffect);
                    }
                    regionEffect(player);
                } else {
                    if (participant.isAlive()) {
                        player.removePotionEffect(PotionEffectType.BLINDNESS);
                    }
                }
            }
        }
    }

    private void regionEffect(Player player) {
        Random random = new Random();
        for (int val = 0; val < 15; val++) {
            double x = (double) random.nextInt(10) + random.nextDouble() - 5.0;
            double z = (double) random.nextInt(10) + random.nextDouble() - 5.0;
            Location location = player.getLocation().add(x, 0, z);
            ParticleEffect.sendPacket(player, location, ParticleEffect.LAVA);
        }
    }

    public void regionWeatherEffect(Player player) {
        if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
            player.setPlayerWeather(WeatherType.CLEAR);
        }
    }

    public void deactivate() {
        for (Participant participant : Participant.getAliveParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (player.hasPotionEffect(PotionEffectType.BLINDNESS)) {
                    player.removePotionEffect(PotionEffectType.BLINDNESS);
                }
            }
        }
    }

    public String regionName() {
        return "Blindness";
    }

}
