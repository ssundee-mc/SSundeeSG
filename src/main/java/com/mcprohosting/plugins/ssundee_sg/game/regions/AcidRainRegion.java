package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Random;

public class AcidRainRegion extends Region {

    private HashMap<String, Long> lastMessage;
    private final Random random;

    public AcidRainRegion(int angle) {
        super(angle);
        random = new Random(System.nanoTime());
    }

    @Override
    public void init() {
        lastMessage = new HashMap<>();
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    regionWeatherEffect(player);
                }
            }
        }
    }

    @Override
    public void tick() {
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    for (int x = -16; x < 16; x++) {
                        for (int y = -16; y < 16; y++) {
                            for (int z = -16; z < 16; z++) {
                                Block block = player.getWorld().getBlockAt(player.getLocation().add(x, y, z));
                                if (block.getType() == Material.LEAVES) {
                                    int chance = random.nextInt(100);
                                    if (chance <= 25) {
                                        block.setType(Material.AIR);
                                    }
                                }
                            }
                        }
                    }
                    int y = player.getWorld().getHighestBlockYAt(player.getLocation());
                    if (y <= player.getLocation().getBlockY()) {
                        if (participant.isAlive() && !participant.checkIfHasEffect(SGItemAction.NO_ACIDRAIN)) {
                            player.damage(1);

                            if (lastMessage.containsKey(player.getName())) {
                                if (System.currentTimeMillis() - lastMessage.get(player.getName()) <= 5000) {//limit message showing to every 5 seconds
                                    continue;
                                }
                            }
                            player.sendMessage("You were damaged by the acid rain.");
                            lastMessage.put(player.getName(), System.currentTimeMillis());
                        }
                    }
                }
            }
        }
    }

    public void regionWeatherEffect(Player player) {
        if (getActivated() != 1) {
            if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
                player.setPlayerWeather(WeatherType.CLEAR);
            }
            return;
        }
        if (player.getPlayerWeather() == WeatherType.CLEAR || player.getPlayerWeather() == null) {
            Participant participant = Participant.getParticipant(player.getName());
            if (!participant.isAlive()) {
                return;
            }
            player.setPlayerWeather(WeatherType.DOWNFALL);
        }
    }

    public void deactivate() {
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
                        player.setPlayerWeather(WeatherType.CLEAR);
                    }
                }
            }
        }
    }

    public String regionName() {
        return "Acid Rain";
    }
}
