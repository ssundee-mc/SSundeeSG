package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.gmail.favorlock.effects.ParticleEffect;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Random;

public class HungerRegion extends Region {

    private HashMap<String, Long> lastMessage;

    public HungerRegion(int angle) {
        super(angle);
    }

    @Override
    public void init() {
        lastMessage = new HashMap<>();
    }

    @Override
    public void tick() {
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (participant.isAlive()) {
                    if (inRegion(player.getLocation())) {
                        if (player.getFoodLevel() < 20) {
                            if (player.getFoodLevel() + 2 > 20) {
                                player.setFoodLevel(20);
                            } else {
                                player.setFoodLevel(player.getFoodLevel() + 2);
                            }
                            if (lastMessage.containsKey(player.getName())) {
                                if (System.currentTimeMillis() - lastMessage.get(player.getName()) <= 5000) {//limit message showing to every 5 seconds
                                    continue;
                                }
                            }
                            player.sendMessage("You gain some food.");
                            lastMessage.put(player.getName(), System.currentTimeMillis());
                        }

                        regionEffect(player);
                    }
                } else {
                    if (inRegion(player.getLocation())) {
                        regionEffect(player);
                    }
                }
            }
        }
    }

    public void regionWeatherEffect(Player player) {
        if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
            player.setPlayerWeather(WeatherType.CLEAR);
        }
    }

    private void regionEffect(Player player) {
        Random random = new Random();
        for (int val = 0; val < 60; val++) {
            double x = (double) random.nextInt(16) + random.nextDouble() - 8.0;
            double z = (double) random.nextInt(16) + random.nextDouble() - 8.0;
            Location location = player.getLocation().add(x, 0.2, z);
            ParticleEffect.sendPacket(player, location, ParticleEffect.HAPPY_VILLAGER);
        }
    }

    public void deactivate() {

    }

    public String regionName() {
        return "Gain Hunger";
    }

}
