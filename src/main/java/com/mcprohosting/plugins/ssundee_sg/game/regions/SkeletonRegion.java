package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WeatherType;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Random;

public class SkeletonRegion extends Region {

    private final Random random;
    private long nextSpawn;
    private ArrayList<LivingEntity> entities = new ArrayList<>();

    public SkeletonRegion(int angle) {
        super(angle);
        random = new Random(System.nanoTime());
    }

    @Override
    public void init() {
        nextSpawn = System.currentTimeMillis();
    }

    @Override
    public void tick() {
        if (System.currentTimeMillis() < nextSpawn) {
            return;
        }
        ArrayList<Participant> inRegion = new ArrayList<>();
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    inRegion.add(participant);
                }
            }
        }

        if (inRegion.isEmpty()) {
            return;
        }

        Participant participant = inRegion.get(random.nextInt(inRegion.size()));

        while (participant.isAlive() == false && listContainsAlivePlayers(inRegion)) {
            participant = inRegion.get(random.nextInt(inRegion.size()));
        }

        Player player = Bukkit.getPlayer(participant.getName());
        if (player != null && participant.isAlive()) {
            int amt = random.nextInt(6);
            for (int i = 0; i < amt; i++) {
                double x = player.getLocation().getX() + (random.nextDouble() - random.nextDouble()) * 8D;
                double y = (player.getLocation().getY() + random.nextInt(7));
                double z = player.getLocation().getZ() + (random.nextDouble() - random.nextDouble()) * 8D;
                Skeleton skeleton = (Skeleton) player.getWorld().spawnEntity(new Location(player.getWorld(), x, y, z), EntityType.SKELETON);
                skeleton.getEquipment().setItemInHand(new ItemStack(Material.BOW));
                entities.add(skeleton);
            }
            nextSpawn = System.currentTimeMillis() + 5000;//5 seconds to next spawn
        }
        for (LivingEntity entity : entities) {
            if (!entity.isDead()) {
                //TODO: untarget target while not in region
                if (!inRegion(entity.getLocation())) {
                    entity.remove();
                }
            }
        }
    }

    public void regionWeatherEffect(Player player) {
        if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
            player.setPlayerWeather(WeatherType.CLEAR);
        }
    }

    public void deactivate() {
        for (LivingEntity entity : entities) {
            if (entity.isDead() == false) {
                entity.remove();
            }
        }
    }

    public String regionName() {
        return "Skeletons";
    }

    public boolean listContainsAlivePlayers(ArrayList<Participant> participants) {
        for (Participant participant : participants) {
            if (participant.isAlive()) {
                return true;
            }
        }

        return false;
    }
}
