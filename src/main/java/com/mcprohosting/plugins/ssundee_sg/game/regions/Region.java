package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.game.logic.Game;
import com.mcprohosting.plugins.ssundee_sg.utils.map.GameMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public abstract class Region {

    private static HashMap<Region, Integer> regions;

    static {
        Random random = new Random(System.nanoTime());

        ArrayList<Integer> angles = new ArrayList<>();
        angles.add(30);
        angles.add(60);
        angles.add(90);
        angles.add(120);
        angles.add(150);
        angles.add(180);
        angles.add(210);
        angles.add(240);
        angles.add(270);
        angles.add(300);
        angles.add(330);
        angles.add(360);

        ArrayList<Region> dumbRegions = new ArrayList<>();
        dumbRegions.add(new SlownessRegion(-1));
        dumbRegions.add(new PoisonRegion(-1));
        dumbRegions.add(new LightningRegion(-1));
        dumbRegions.add(new SkeletonRegion(-1));
        dumbRegions.add(new ZombieRegion(-1));
        dumbRegions.add(new BlindnessRegion(-1));
        dumbRegions.add(new AcidRainRegion(-1));
        dumbRegions.add(new WeaknessRegion(-1));
        dumbRegions.add(new ResistanceRegion(-1));
        dumbRegions.add(new HungerRegion(-1));
        dumbRegions.add(new HealthRegion(-1));
        dumbRegions.add(new SpeedRegion(-1));

        regions = new HashMap<>();

        while (dumbRegions.isEmpty() == false) {
            Region region = dumbRegions.remove(random.nextInt(dumbRegions.size()));
            int angle = angles.remove(0);
            region.setAngle(angle);
            int id = regions.size() + 1;
            regions.put(region, id);
        }

        /*regions.put(new SlownessRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new PoisonRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new LightingRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new WolfRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new ZombieRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new BlindnessRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new AcidRainRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new WeaknessRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new ResistanceRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new HungerRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new HealthRegion(angles.remove(random.nextInt(angles.size()))), regions.size());
        regions.put(new SpeedRegion(angles.remove(random.nextInt(angles.size()))), regions.size());*/
    }

    private int angle;
    private int activated;
    private int runTime;
    private BukkitTask task;

    public Region(int angle) {
        this.angle = angle;
        this.activated = -1;
        this.task = null;
    }

    public static HashMap<Region, Integer> getRegions() {
        return regions;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getRunTime() {
        return runTime;
    }

    public int getActivated() {
        return activated;
    }

    public void countDownRegion() {
        System.out.println("Starting count down...");
        activated = 0;
        final Region region = this;
        task = Bukkit.getScheduler().runTaskTimer(SsundeeSG.getPlugin(), new Runnable() {

            int seconds = 5;
            long lastTick = 0;

            @Override
            public void run() {
                if (System.currentTimeMillis() - lastTick < 900) {//make sure ~1 second has past
                    return;
                }
                lastTick = System.currentTimeMillis();
                if (activated == 1) {
                    if (shouldCancel() == true) {
                        return;
                    }
                    tick();
                    return;
                }
                if (seconds == 5) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Sector " + getRegions().get(region) + " activating in 5 seconds.");
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.playSound(player.getLocation(), Sound.ORB_PICKUP, 10.0F, 1.0F);
                    }
                } else if (seconds != 0) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "" + seconds + "...");
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.playSound(player.getLocation(), Sound.ORB_PICKUP, 10.0F, 1.0F);
                    }
                } else {
                    activated = 1;
                    runTime = 120;
                    init();
                    Bukkit.broadcastMessage(ChatColor.GREEN + "Activated Sector " + getRegions().get(region));
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.playSound(player.getLocation(), Sound.ANVIL_LAND, 10.0F, 1.0F);
                    }
                }
                seconds -= 1;
            }
        }, 0L, 20L);
    }

    public void stopRegion() {
        task.cancel();
        deactivate();
        activated = 2;
    }

    private double calcAngle(Location location, Location center) {

        Vector centerVector = new Vector(center.getBlockX(), 0, center.getBlockZ());
        Vector playerVector = new Vector(location.getX(), 0, location.getZ());

        double angle = Math.toDegrees(Math.atan2(playerVector.getX() - centerVector.getX(), playerVector.getZ() - centerVector.getZ()));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

    public void init() {

    }

    public abstract void deactivate();

    public abstract void regionWeatherEffect(Player player);

    public abstract void tick();

    public abstract String regionName();

    public boolean inRegion(Location location) {
        GameMap map = null;
        if (SsundeeSG.getPlugin().getGame().getMap() == null) {
            for (GameMap gameMap : Game.getMaps().keySet()) {
                if (gameMap.getMapName().equalsIgnoreCase(location.getWorld().getName())) {
                    map = gameMap;
                }
            }
            if (map == null) {
                return false;
            }
        } else {
            map = SsundeeSG.getPlugin().getGame().getMap();
            if (map.getMapCenter().getWorld() != location.getWorld()) {
                return false;
            }
        }
        //radius of 15
        double dis2 = NumberConversions.square(map.getMapCenter().getX() - location.getX()) + NumberConversions.square(map.getMapCenter().getZ() - location.getZ());
        if (Math.sqrt(dis2) <= 15) {
            return false;
        }
        if (calcAngle(location, map.getMapCenter()) >= (getAngle() - 30) && calcAngle(location, map.getMapCenter()) <= getAngle()) {
            return true;
        }
        return false;
    }

    public boolean shouldCancel() {
        if (runTime <= 0) {
            Bukkit.broadcastMessage(ChatColor.RED + "Sector " + getRegions().get(this) + " deactivating.");
            stopRegion();
            return true;
        }
        if (runTime == 5) {
            Bukkit.broadcastMessage(ChatColor.GOLD + "Sector " + getRegions().get(this) + " deactivating in 5 seconds.");
        }
        runTime -= 1;
        return false;
    }

}
