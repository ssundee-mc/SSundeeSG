package com.mcprohosting.plugins.ssundee_sg.game.logic;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.config.WorldInfoConfig;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.game.items.ItemChest;
import com.mcprohosting.plugins.ssundee_sg.game.regions.Region;
import com.mcprohosting.plugins.ssundee_sg.utils.ItemUtils;
import com.mcprohosting.plugins.ssundee_sg.utils.LocationUtils;
import com.mcprohosting.plugins.ssundee_sg.utils.bossbar.FakeDragon;
import com.mcprohosting.plugins.ssundee_sg.utils.map.GameMap;
import com.mcprohosting.plugins.ssundee_sg.utils.map.MapAutoSave;
import com.mcprohosting.plugins.ssundee_sg.utils.map.Spawn;
import com.mcprohosting.plugins.ssundee_sg.utils.map.WorldManager;
import com.mcprohosting.plugins.ssundee_sg.utils.server.LaunchFirework;
import com.mcprohosting.plugins.ssundee_sg.utils.server.MoveServer;
import com.mcprohosting.plugins.ssundee_sg.utils.server.Shutdown;
import com.mcprohosting.plugins.ssundee_sg.utils.server.UpdateTab;
import org.bukkit.*;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.NumberConversions;

import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Game implements Runnable {

    private static final int limBot = 1;
    private static Map<GameMap, Integer> maps;

    static {
        maps = new HashMap<>();
    }

    private final Random RANDOM;
    private final int START_COUNTDOWN;

    private GameMap map;
    private GameMap dm;
    private int seconds;
    private GameState status;
    private boolean votingInitiated;
    private boolean teleported;
    private boolean started;
    private boolean ended;
    private long lastTick;
    private long lastAutoSave;
    private Scoreboard scoreboard;
    private boolean reFillChests;

    public Game() {
        RANDOM = new Random(System.nanoTime());
        START_COUNTDOWN = 60;

        map = null;
        votingInitiated = false;
        teleported = false;
        started = false;
        ended = false;
        seconds = -1;
        lastTick = 0;
        lastAutoSave = 0;
        reFillChests = false;
    }

    public static Map<GameMap, Integer> getMaps() {
        return maps;
    }

    public GameMap getMap() {
        return map;
    }

    public void setMap(GameMap map) {
        this.map = map;
    }

    public int getSeconds() {
        return seconds;
    }

    public GameState getStatus() {
        return status;
    }

    public void setStatus(GameState status) {
        this.status = status;
        DatabaseManager.updateStatus();
    }

    public void forceDeathMatch() {
        seconds = 840;
        setStatus(GameState.INPROGRESS);
    }

    public void forceStart() {
        if (Game.getMaps().size() <= 0) {
            if (DatabaseManager.getMaps().size() <= 0) {
                Bukkit.broadcastMessage(ChatColor.RED + "No maps have been added to the database. Please let a staff member know!");
                return;
            }
            for (GameMap map : DatabaseManager.getMaps()) {
                Game.getMaps().put(map, 0);
            }
        }
        if (map == null) {
            int votes = -1;
            for (GameMap m : maps.keySet()) {
                if (maps.get(m) > votes) {
                    votes = maps.get(m);
                    map = m;
                }
            }


            if (WorldManager.importWorld(this.map.getMapName(), true) == true) {
                Bukkit.broadcastMessage(ChatColor.RED + "Error importing map " + map.getMapName() + " please let a staff member know!");
                return;
            }
            World world = SsundeeSG.getPlugin().getServer().createWorld(new WorldCreator(map.getMapName()));
            map.getMapCenter().setWorld(world);
            int mapID = map.getId();
            for (String s : DatabaseManager.getSpawns(mapID)) {
                Location loc = LocationUtils.StringToLocation(s);
                map.getSpawns().put(map.getSpawns().size(), new Spawn(map.getSpawns().size(), loc));
            }
            for (String loc : DatabaseManager.getChests(mapID)) {
                Location location = LocationUtils.StringToLocation(loc);
                new ItemChest(location);//don't need to add to map?
            }
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
            }
        }
        seconds = START_COUNTDOWN;
        setStatus(GameState.STARTING);
    }

    public void mapScoreboard(int seconds) {
        if (scoreboard == null) {
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        Objective obj = scoreboard.getObjective("maps");
        if (obj == null) {
            obj = scoreboard.registerNewObjective("maps", "dummy");
        }
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        String time = calcTime(seconds);
        obj.setDisplayName("Voting " + time);
        for (GameMap map : maps.keySet()) {
            obj.getScore(Bukkit.getOfflinePlayer(map.getMapName())).setScore(maps.get(map));
        }
    }

    private String calcTime(int seconds) {
        return String.format("%02d:%02d",
                TimeUnit.SECONDS.toMinutes(seconds),
                seconds - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds))
        );
    }

    public void gameScoreboard(Player player, int seconds) {
        if (player.getScoreboard() == null || player.getScoreboard() == Bukkit.getScoreboardManager().getMainScoreboard()) {
            player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        }
        if (player.getScoreboard().getObjective(DisplaySlot.SIDEBAR) == null) {
            Objective obj = player.getScoreboard().registerNewObjective("game", "dumy");
            obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        }
        Objective obj = player.getScoreboard().getObjective(DisplaySlot.SIDEBAR);

        String time = calcTime(seconds);
        if (status == GameState.MAINTENANCE) {
            obj.setDisplayName("Maintenance Mode");
        } else if (seconds >= 900) {
            time = calcTime(1080 - seconds);
            obj.setDisplayName("Game Over " + time);
        } else {
            obj.setDisplayName("Game Time " + time);
        }

        Region inRegion = null;
        for (Region region : Region.getRegions().keySet()) {
            if (region.inRegion(player.getLocation())) {
                inRegion = region;
                break;
            }
        }

        if (inRegion != null) {
            Participant participant = Participant.getParticipant(player.getName());
            if (participant.isAlive()) {
                FakeDragon.setStatus(player, ChatColor.RED + "You are in Sector " + Region.getRegions().get(inRegion), 100);
            } else {
                FakeDragon.setStatus(player, ChatColor.RED + "Spectating in Sector " + Region.getRegions().get(inRegion), 100);
            }
        } else {
            FakeDragon.setStatus(player, "", 100);
        }

        if (status == GameState.MAINTENANCE) {
            obj.getScore(Bukkit.getOfflinePlayer("To teleport use")).setScore(Bukkit.getWorlds().size() + 3);
            obj.getScore(Bukkit.getOfflinePlayer("/mtp [worldName]")).setScore(Bukkit.getWorlds().size() + 2);
            obj.getScore(Bukkit.getOfflinePlayer("Current Worlds:")).setScore(Bukkit.getWorlds().size() + 1);
            for (int i = Bukkit.getWorlds().size(); i > 0; i--) {
                World world = Bukkit.getWorlds().get(i - 1);
                if (world.getName().length() > 16) {
                    obj.getScore(Bukkit.getOfflinePlayer(world.getName().substring(0, 16))).setScore(i);
                } else {
                    obj.getScore(Bukkit.getOfflinePlayer(world.getName())).setScore(i);
                }
            }
            return;
        }

        ArrayList<OfflinePlayer> toReset = new ArrayList<>();

        for (OfflinePlayer offlinePlayer : player.getScoreboard().getPlayers()) {
            toReset.add(offlinePlayer);
        }


        ArrayList<Region> activeRegions = new ArrayList<>();
        for (Region region : Region.getRegions().keySet()) {
            if (region.getActivated() == 1) {
                activeRegions.add(region);
            }
        }
        if (activeRegions.isEmpty()) {
            for (OfflinePlayer offlinePlayer : toReset) {
                if (offlinePlayer.getName().equalsIgnoreCase("")) {
                    continue;
                }
                player.getScoreboard().resetScores(offlinePlayer);
            }
            obj.getScore(Bukkit.getOfflinePlayer("")).setScore(1);
            return;
        }
        Region region = activeRegions.get(0);

        for (OfflinePlayer offlinePlayer : toReset) {
            if (region != null) {
                if (offlinePlayer.getName().equalsIgnoreCase(ChatColor.GREEN + "Sector " + Region.getRegions().get(region) + "")) {
                    continue;
                }
                if (offlinePlayer.getName().equalsIgnoreCase(ChatColor.GOLD + region.regionName())) {
                    continue;
                }
            }
            if (activeRegions.size() == 2) {
                if (offlinePlayer.getName().equalsIgnoreCase(ChatColor.GREEN + "Sector " + Region.getRegions().get(activeRegions.get(1)) + "")) {
                    continue;
                }
                if (offlinePlayer.getName().equalsIgnoreCase(ChatColor.GOLD + activeRegions.get(1).regionName())) {
                    continue;
                }
            }
            player.getScoreboard().resetScores(offlinePlayer);
        }

        if (region != null) {
            obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Sector " + Region.getRegions().get(region) + "")).setScore(7);
            time = calcTime(region.getRunTime());
            obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GOLD + region.regionName())).setScore(6);
            obj.getScore(Bukkit.getOfflinePlayer(time)).setScore(5);
        }
        obj.getScore(Bukkit.getOfflinePlayer("")).setScore(5);
        if (activeRegions.size() == 2) {
            region = activeRegions.get(1);
            obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Sector " + Region.getRegions().get(region) + "")).setScore(3);
            time = calcTime(region.getRunTime());
            obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GOLD + region.regionName())).setScore(2);
            obj.getScore(Bukkit.getOfflinePlayer(time)).setScore(1);
        }
    }

    //these material IDs are acceptable for places to teleport player; breathable blocks and water
    private static final LinkedHashSet<Integer> safeOpenBlocks = new LinkedHashSet<Integer>(Arrays.asList(
            new Integer[]{0, 6, 8, 9, 27, 28, 30, 31, 32, 37, 38, 39, 40, 50, 55, 59, 63, 64, 65, 66, 68, 69, 70, 71, 72, 75, 76, 77, 78, 83, 90, 93, 94, 96, 104, 105, 106, 115, 131, 132, 141, 142, 149, 150, 157, 171}
    ));

    //these material IDs are ones we don't want to drop the player onto, like cactus or lava or fire or activated Ender portal
    private static final LinkedHashSet<Integer> painfulBlocks = new LinkedHashSet<Integer>(Arrays.asList(
            new Integer[]{10, 11, 51, 81, 119}
    ));

    private boolean isSafeSpot(World world, int X, int Y, int Z, boolean flying) {
        boolean safe = safeOpenBlocks.contains((Integer) world.getBlockTypeIdAt(X, Y, Z))                // target block open and safe
                && safeOpenBlocks.contains((Integer) world.getBlockTypeIdAt(X, Y + 1, Z));        // above target block open and safe
        if (!safe || flying)
            return safe;

        Integer below = (Integer) world.getBlockTypeIdAt(X, Y - 1, Z);
        return (safe
                && (!safeOpenBlocks.contains(below) || below == 8 || below == 9)        // below target block not open/breathable (so presumably solid), or is water
                && !painfulBlocks.contains(below)                                                                        // below target block not painful
        );
    }

    // find closest safe Y position from the starting position
    private double getSafeY(World world, int X, int Y, int Z, boolean flying) {
        // artificial height limit of 127 added for Nether worlds since CraftBukkit still incorrectly returns 255 for their max height, leading to players sent to the "roof" of the Nether
        final int limTop = (world.getEnvironment() == World.Environment.NETHER) ? 125 : world.getMaxHeight() - 2;
        // Expanding Y search method adapted from Acru's code in the Nether plugin

        for (int y1 = Y, y2 = Y; (y1 > limBot) || (y2 < limTop); y1--, y2++) {
            // Look below.
            if (y1 > limBot) {
                if (isSafeSpot(world, X, y1, Z, flying))
                    return (double) y1;
            }

            // Look above.
            if (y2 < limTop && y2 != y1) {
                if (isSafeSpot(world, X, y2, Z, flying))
                    return (double) y2;
            }
        }

        return -1.0;        // no safe Y location?!?!? Must be a rare spot in a Nether world or something
    }

    // find closest safe Y position from the starting position
    private double getSafeY(Location location, boolean flying) {
        // artificial height limit of 127 added for Nether worlds since CraftBukkit still incorrectly returns 255 for their max height, leading to players sent to the "roof" of the Nether
        final int limTop = (location.getWorld().getEnvironment() ==
                World.Environment.NETHER) ? 125 : location.getWorld().getMaxHeight() - 2;
        // Expanding Y search method adapted from Acru's code in the Nether plugin

        for (int y = location.getBlockY(); y < limTop; y++) {
            // Look above.
            if (y < limTop) {
                if (isSafeSpot(location.getWorld(), location.getBlockX(), y, location.getBlockZ(), flying))
                    return (double) y;
            }
        }

        return -1.0;        // no safe Y location?!?!? Must be a rare spot in a Nether world or something
    }

    @Override
    public void run() {
        if (System.currentTimeMillis() - lastTick < 900) {//make sure ~1 second has past
            return;
        }
        lastTick = System.currentTimeMillis();
        // LOBBY LOGIC
        if (status == GameState.LOBBY) {
            if (Bukkit.getOnlinePlayers().length < SsundeeSG.grabConfig().settings_minplayers) {
                if (seconds > 20) {
                    Bukkit.broadcastMessage(ChatColor.RED + "A minimum of " + SsundeeSG.grabConfig().settings_minplayers + " tributes is required to depart.");
                    seconds = 0;
                }
                seconds += 1;
            } else {
                seconds = 0;
                setStatus(GameState.VOTING);
            }
        }
        // VOTING LOGIC
        if (status == GameState.VOTING) {
            if (maps.size() == 1) {//Do not vote if there is only one map
                setStatus(GameState.POSTVOTING);
                return;
            }
            mapScoreboard(seconds);
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setScoreboard(scoreboard);
            }
            if (votingInitiated) {
                if (seconds % 30 == 0) {
                    String time = calcTime(seconds);
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Voting ends in " + time);
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Please vote for the maps using /vote [mapName]");
                }
                seconds -= 1;
                if (seconds <= 0) {//wait 2 minutes to pick a map
                    setStatus(GameState.POSTVOTING);
                }
            } else {
                votingInitiated = !votingInitiated;
                seconds = 120;
                Bukkit.broadcastMessage(ChatColor.GOLD + "Two minutes left for map voting.");
                Bukkit.broadcastMessage(ChatColor.GOLD + "Please vote for the maps using /vote [mapName]");
            }
        }
        if (status == GameState.POSTVOTING) {
            if (map == null) {
                int votes = -1;
                for (GameMap map : maps.keySet()) {
                    if (maps.get(map) > votes) {
                        votes = maps.get(map);
                        this.map = map;
                    }
                }

                if (WorldManager.importWorld(this.map.getMapName(), true)) {
                    Bukkit.broadcastMessage(ChatColor.RED + "Error importing map " + map.getMapName() + " please let a staff member know!");
                    return;
                }
                World world = SsundeeSG.getPlugin().getServer().createWorld(new WorldCreator(map.getMapName()));
                map.getMapCenter().setWorld(world);
                int mapID = map.getId();
                for (String s : DatabaseManager.getSpawns(mapID)) {
                    Location loc = LocationUtils.StringToLocation(s);
                    map.getSpawns().put(map.getSpawns().size(), new Spawn(map.getSpawns().size(), loc));
                }
                for (String loc : DatabaseManager.getChests(mapID)) {
                    Location location = LocationUtils.StringToLocation(loc);
                    new ItemChest(location);//don't need to add to map?
                }
                Bukkit.broadcastMessage(ChatColor.GOLD + "The train will depart to " + map + " in 1 minute!");
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
                }
                seconds = 60;
            }
            if (seconds > 0) {
                if (seconds == 30) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "The train will depart to " + map + " in 30 seconds!");
                }
                if (seconds <= 5) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "The train will depart in " + seconds + " seconds.");
                }
                seconds -= 1;
            }
            if (seconds == 0) {
                seconds = START_COUNTDOWN;
                setStatus(GameState.STARTING);
            }
        }
        // STARTING LOGIC
        if (status == GameState.STARTING) {
            if (seconds == 60) {
                WorldInfoConfig config = new WorldInfoConfig(SsundeeSG.getPlugin(),
                        new File(map.getMapName()));
                try {
                    config.init();
                } catch (InvalidConfigurationException e) {
                    SsundeeSG.getPlugin().getLogger().info(ChatColor.RED +
                            "Failed to initialize world info configuration!");
                }

                if (config.getMessage().equalsIgnoreCase("") == false) {
                    Bukkit.broadcastMessage(ChatColor.GREEN + config.getMessage());
                }

                Bukkit.getWorld(map.getMapName()).setTime(0);
                Bukkit.getWorld(map.getMapName()).setThundering(false);
                for (Participant p : Participant.getAliveParticipants()) {
                    p.isTribute(true);
                }
            }

            if (!teleported) {
                teleportPlayers(this.map, true);
                Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new UpdateTab());
                teleported = !teleported;
            }

            if (seconds > 0) {
                if (seconds <= 10) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Starting in " + seconds + "... ");
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.playSound(player.getLocation(), Sound.NOTE_PIANO, 10.0F, 1.0F);
                    }
                } else {
                    if (seconds % 10 == 0) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Starting in " + seconds + "... ");
                    }
                }
                seconds -= 1;
            }

            if (seconds == 0) {//record the time the game starts
                teleported = false;//use for death match teleporting
                Bukkit.broadcastMessage(ChatColor.GOLD + "Match Started!");
                Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new UpdateTab());
                for (Participant p : Participant.getAliveParticipants()) {
                    if (p.isTribute()) {
                        Bukkit.getPlayer(p.getName()).getInventory().clear();
                        p.setOwnedItems(DatabaseManager.getUserItemsFromDatabase(p.getPlayerId()));
                        Bukkit.getPlayer(p.getName()).getInventory().setItem(8,
                                ItemUtils.generatePaperWithMetaForPlayer(Bukkit.getPlayer(p.getName())));
                    }
                }
                setStatus(GameState.INPROGRESS);
            }
        }
        // INPROGRESS LOGIC
        if (status == GameState.INPROGRESS) {
            if (started == false) {
                started = true;
                for (Participant participant : Participant.getAliveParticipants()) {
                    DatabaseManager.updatePlayerTotalGames(participant.getPlayerId());
                }
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                Location loc = player.getLocation();
                double dis2 = NumberConversions.square(map.getMapCenter().getX() - player.getLocation().getX()) +
                        NumberConversions.square(map.getMapCenter().getZ() - player.getLocation().getZ());
                if (Math.sqrt(dis2) >= 600 && seconds < 900) {//don't allow walking past 600 from center
                    double xLoc = loc.getX();
                    double zLoc = loc.getZ();
                    double yLoc = loc.getY();
                    double dX = xLoc - map.getMapCenter().getX();
                    double dZ = zLoc - map.getMapCenter().getX();
                    double dU = Math.sqrt(dX * dX + dZ * dZ); //distance of the untransformed point from the center
                    double dT = Math.sqrt(dX * dX / 360000 + dZ * dZ / 360000); //distance of the transformed point from the center
                    double f = (1 / dT - 3.0 / dU); //"correction" factor for the distances
                    xLoc = map.getMapCenter().getX() + dX * f;
                    zLoc = map.getMapCenter().getZ() + dZ * f;
                    int ixLoc = Location.locToBlock(xLoc);
                    int izLoc = Location.locToBlock(zLoc);
                    yLoc = getSafeY(loc.getWorld(), ixLoc, Location.locToBlock(yLoc), izLoc, player.isFlying());
                    loc = new Location(loc.getWorld(), Math.floor(xLoc) + 0.5, yLoc, Math.floor(zLoc) + 0.5, loc.getYaw(), loc.getPitch());
                    player.teleport(loc);
                }

                if (seconds >= 900 && teleported) {
                    if (dm == map) {
                        if (Math.sqrt(dis2) >= 50) {//don't allow walking past 50 from center for deathmatch
                            double xLoc = loc.getX();
                            double zLoc = loc.getZ();
                            double yLoc = loc.getY();
                            double dX = xLoc - map.getMapCenter().getX();
                            double dZ = zLoc - map.getMapCenter().getX();
                            double dU = Math.sqrt(dX * dX + dZ * dZ); //distance of the untransformed point from the center
                            double dT = Math.sqrt(dX * dX / 2500 + dZ * dZ / 2500); //distance of the transformed point from the center
                            double f = (1 / dT - 3.0 / dU); //"correction" factor for the distances
                            xLoc = map.getMapCenter().getX() + dX * f;
                            zLoc = map.getMapCenter().getZ() + dZ * f;
                            int ixLoc = Location.locToBlock(xLoc);
                            int izLoc = Location.locToBlock(zLoc);
                            yLoc = getSafeY(loc.getWorld(), ixLoc, Location.locToBlock(yLoc), izLoc, player.isFlying());
                            loc = new Location(loc.getWorld(), Math.floor(xLoc) + 0.5, yLoc, Math.floor(zLoc) + 0.5, loc.getYaw(), loc.getPitch());
                            player.teleport(loc);
                        }
                    }
                }
            }

            if (seconds >= 90 && seconds <= 840) {//between 1:30 and 14 minutes
                if (((seconds) + 5) % 60 == 0) {//exactly 5 seconds before minute
                    ArrayList<Region> regions = new ArrayList<>();
                    for (Region region : Region.getRegions().keySet()) {
                        if (region.getActivated() == -1) {
                            regions.add(region);
                        }
                    }
                    if (!regions.isEmpty()) {
                        Region region = regions.get(RANDOM.nextInt(regions.size()));
                        if (region != null) {
                            region.countDownRegion();
                        }
                    }
                }
            }

            if (seconds == 870) {
                dm = WorldManager.getDeathmatchMap();

                if (dm != null) {
                    if (WorldManager.importWorld(dm.getMapName(), true)) {
                        Bukkit.broadcastMessage(ChatColor.RED + "Error importing map " + dm.getMapName() + " please let a staff member know!");
                        return;
                    } else {
                        World world = SsundeeSG.getPlugin().getServer().createWorld(new WorldCreator(dm.getMapName()));
                        world.setSpawnLocation(dm.getMapCenter().getBlockX(),
                                dm.getMapCenter().getBlockY(),
                                dm.getMapCenter().getBlockZ());
                        world.setKeepSpawnInMemory(true);
                        dm.getMapCenter().setWorld(world);
                    }
                } else {
                    dm = this.map;
                }
            }

            if (seconds == 900) {//at 15 minutes
                if (!teleported) {//activate deathmatch
                    Bukkit.broadcastMessage(ChatColor.RED + "The Death Match has started!");
                    teleportPlayers(dm, false);
                    teleported = true;
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.removePotionEffect(PotionEffectType.BLINDNESS);
                        player.removePotionEffect(PotionEffectType.REGENERATION);
                        player.removePotionEffect(PotionEffectType.POISON);
                        player.removePotionEffect(PotionEffectType.HUNGER);
                        player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                        player.removePotionEffect(PotionEffectType.SLOW);
                        player.removePotionEffect(PotionEffectType.WEAKNESS);
                    }
                    return;
                }
            }

            if (seconds >= 600 && !teleported) {//10 minutes start counting down
                if (reFillChests == false) {
                    for (String loc : DatabaseManager.getChests(map.getId())) {
                        Location location = LocationUtils.StringToLocation(loc);
                        new ItemChest(location);//don't need to add to map?
                    }
                    reFillChests = true;
                }
            }

            if (840 <= seconds && seconds < 900) {
                if ((900 - seconds) <= 10) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Deathmatch in " + (900 - seconds) + "... ");
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.playSound(player.getLocation(), Sound.NOTE_PIANO, 10.0F, 1.0F);
                    }
                } else {
                    if ((900 - seconds) % 10 == 0) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Deathmatch in " + (900 - seconds) + "... ");
                    }
                }
            }

            if (seconds >= 960) {//16 minutes start counting down
                if ((seconds) % 60 == 0) {//exactly on the minute
                    if (seconds == 1020) {//17 minutes
                        Bukkit.broadcastMessage(ChatColor.GOLD + "The game will end in " + (1080 - seconds) / 60 + " minute!");
                    } else {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "The game will end in " + (1080 - seconds) / 60 + " minutes!");
                    }
                }
            }

            if (seconds >= 1080) {//at 18 minutes
                setStatus(GameState.GAMEOVER);//end the game
                return;//don't need to check anything under this
            }

            if (Participant.getAliveParticipants().size() <= 1) {
                setStatus(GameState.GAMEOVER);//end the game
                return;
            }
            for (Player player : Bukkit.getOnlinePlayers()) {
                gameScoreboard(player, seconds);
            }
            // Do Stuff
            seconds += 1;
        }
        if (status == GameState.MAINTENANCE) {
            if (lastAutoSave == 0) {
                lastAutoSave = System.currentTimeMillis();
            }

            for (GameMap map : maps.keySet()) {
                if (Bukkit.getServer().getWorld(map.getMapName()) == null) {
                    if (!WorldManager.importWorld(map.getMapName(), false)) {
                        Bukkit.broadcastMessage(ChatColor.RED + "Error importing map " + map.getMapName() + " please let a staff member know!");
                        return;
                    }
                    World world = SsundeeSG.getPlugin().getServer().createWorld(new WorldCreator(map.getMapName()));
                    map.getMapCenter().setWorld(world);
                    int mapID = map.getId();
                    for (String s : DatabaseManager.getSpawns(mapID)) {
                        Location loc = LocationUtils.StringToLocation(s);
                        map.getSpawns().put(map.getSpawns().size(), new Spawn(map.getSpawns().size(), loc));
                    }
                    for (String loc : DatabaseManager.getChests(mapID)) {
                        Location location = LocationUtils.StringToLocation(loc);
                        new ItemChest(location);//don't need to add to map?
                    }
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Maintenance Mode Loaded Map " + map.getMapName());
                }
            }
            for (String mapName : WorldManager.getAvailableMaps()) {
                if (Bukkit.getServer().getWorld(mapName) == null) {
                    WorldManager.importWorld(mapName, false);
                    World world = SsundeeSG.getPlugin().getServer().createWorld(new WorldCreator(mapName));
                    Bukkit.broadcastMessage(ChatColor.GREEN + "Maintenance Mode Imported New Map " + mapName + " that has not been added yet");
                }
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                gameScoreboard(player, seconds);
            }

            if ((System.currentTimeMillis() - lastAutoSave) > (10 * 60 * 1000)) {
                Bukkit.getScheduler().scheduleSyncRepeatingTask(SsundeeSG.getPlugin(), new MapAutoSave(), 600 * 20, 0);
                lastAutoSave = System.currentTimeMillis();
            }
        }
        // GAMEOVER LOGIC
        if (status == GameState.GAMEOVER) {
            if (!ended) {
                if (Participant.getAliveParticipants().size() == 0) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + "There was no winner.");
                } else if (Participant.getAliveParticipants().size() == 1) {
                    Bukkit.broadcastMessage(ChatColor.GOLD + Participant.getAliveParticipants().get(0).getName() + " has won!");
                    Player winner = Bukkit.getPlayer(Participant.getAliveParticipants().get(0).getName());
                    Participant.getAliveParticipants().get(0).setPlace();
                    for (int x = 0; x < 5; x++) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(),
                                new LaunchFirework(winner.getLocation()), x * 20);
                    }
                } else {
                    StringBuilder builder = new StringBuilder();
                    for (Participant participant : Participant.getAliveParticipants()) {
                        builder.append(participant.getName()).append(", ");
                        participant.setPlace();
                    }
                    String winners = builder.toString();
                    winners = winners.substring(0, winners.length() - 2);//get rid of trailing space and ,
                    Bukkit.broadcastMessage(ChatColor.GOLD + winners + " have won!");
                }
                for (Participant participant : Participant.getParticipants()) {
                    int id = participant.getPlayerId();
                    switch (participant.getPlace()) {
                        case 1:
                            DatabaseManager.addTickets(id, 50);
                            DatabaseManager.addPoints(id, 100);
                            DatabaseManager.updatePlayerWins(id);
                            break;
                        case 2:
                            DatabaseManager.addTickets(id, 25);
                            break;
                        case 3:
                            DatabaseManager.addTickets(id, 10);
                            break;
                        case 4:
                            DatabaseManager.addTickets(id, 5);
                            break;
                        default:
                            break;
                    }
                }

                setStatus(GameState.RESTARTING);

                Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            player.setFoodLevel(20);
                            player.setHealth(20.0);
                            player.setTotalExperience(0);
                            player.setGameMode(GameMode.SURVIVAL);
                            player.getInventory().clear();
                            player.getInventory().setArmorContents(null);
                            player.getActivePotionEffects().clear();
                            player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                        }
                    }
                }, 110);
                Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new MoveServer(), 200);
                Bukkit.getScheduler().scheduleSyncDelayedTask(SsundeeSG.getPlugin(), new Shutdown(), 300);
                ended = true;
            }
        }
    }

    public void teleportPlayers(GameMap map, boolean reset) {
        int size = map.getSpawns().size();
        int position = 0;
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if(player == null) {
                continue;
            }

            if (reset) {
                player.setFoodLevel(20);
                player.setHealth(20.0);
                player.setTotalExperience(0);
                player.setGameMode(GameMode.SURVIVAL);
                player.getInventory().clear();
                player.getActivePotionEffects().clear();
                player.getInventory().setArmorContents(null);
            }

            if (map == this.map) {
                if (position < size) {
                    Location location = map.getSpawns().get(position).getLocation(map.getMapName());
                    map.getSpawns().get(position).setOccupied(true);
                    location = location.add(0.5, 0.5, 0.5);
                    player.teleport(location);
                    participant.setSpawnId(position);
                    position++;
                } else {
                    player.sendMessage(ChatColor.RED + "Not enough spawn pads available. You have been set to spectator mode.");
                    Participant.getParticipant(player.getName()).setDead();
                }
            } else {
                Location location = getRandomLocationWithinCircle(map.getRadius(), map.getMapCenter()).add(0.5, 0.5, 0.5);
                player.teleport(location);
            }
        }
    }

    private Location getRandomLocationWithinCircle(int radius, Location location) {
        Random random = new Random();

        int randomDegree = random.nextInt(360);
        int randomRadius = random.nextInt(radius);
        double x = Math.cos(randomDegree);
        double z = Math.sin(randomDegree);
        double y;

        x *= randomRadius;
        z *= randomRadius;

        x += location.getX();
        z += location.getZ();
        y = getSafeY(location, false);

        return new Location(location.getWorld(), (int)x, (int)y, (int)z, random.nextFloat() * 360.0f, 0.0f);
    }

}
