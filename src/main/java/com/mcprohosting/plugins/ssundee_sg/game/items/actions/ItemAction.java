package com.mcprohosting.plugins.ssundee_sg.game.items.actions;

import org.bukkit.entity.Player;

public interface ItemAction {

    public abstract void playerInteract(Player player);

}
