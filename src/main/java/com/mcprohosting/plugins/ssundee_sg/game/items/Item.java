package com.mcprohosting.plugins.ssundee_sg.game.items;

import com.mcprohosting.plugins.ssundee_sg.config.ConfigObject;
import org.bukkit.inventory.ItemStack;

public class Item extends ConfigObject {

    public int itemId;
    public int data = 0;
    public int amount = 1;
    public int itemValue;

    public Item() {
    }

    public Item(int itemId, int data, int amount, int itemValue) {
        this.itemId = itemId;
        this.data = data;
        this.amount = amount;
        this.itemValue = itemValue;
    }

    public ItemStack getItemStack() {
        return new ItemStack(itemId, amount, (short) data);
    }

}


