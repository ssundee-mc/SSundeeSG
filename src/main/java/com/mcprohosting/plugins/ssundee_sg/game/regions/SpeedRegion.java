package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.gmail.favorlock.effects.ParticleEffect;
import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class SpeedRegion extends Region {

    public SpeedRegion(int angle) {
        super(angle);
    }

    @Override
    public void init() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void tick() {
        for (Participant participant : Participant.getParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (participant.isAlive()) {
                    if (inRegion(player.getLocation())) {
                        if (!player.hasPotionEffect(PotionEffectType.SPEED)) {
                            player.sendMessage(ChatColor.GREEN + "You were given speed boost.");
                        }
                        PotionEffect potionEffect = new PotionEffect(PotionEffectType.SPEED, 80, 1);
                        player.addPotionEffect(potionEffect, true);

                        regionEffect(player);
                    }
                } else {
                    if (inRegion(player.getLocation())) {
                        regionEffect(player);
                    }
                }
            }
        }
    }

    public void regionWeatherEffect(Player player) {
        if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
            player.setPlayerWeather(WeatherType.CLEAR);
        }
    }

    private void regionEffect(Player player) {
        Random random = new Random();
        for (int val = 0; val < 60; val++) {
            double x = (double) random.nextInt(16) + random.nextDouble() - 8.0;
            double z = (double) random.nextInt(16) + random.nextDouble() - 8.0;
            Location location = player.getLocation().add(x, 0.4, z);
            ParticleEffect.sendPacket(player, location, ParticleEffect.MAGIC_CRIT);
        }
    }

    public void deactivate() {

    }

    public String regionName() {
        return "Speed";
    }

}
