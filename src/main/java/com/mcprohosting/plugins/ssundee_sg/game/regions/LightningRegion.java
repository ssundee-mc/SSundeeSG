package com.mcprohosting.plugins.ssundee_sg.game.regions;

import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.items.SGItemAction;
import org.bukkit.Bukkit;
import org.bukkit.WeatherType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Random;

public class LightningRegion extends Region {

    private final Random random;
    private long nextStrike;

    public LightningRegion(int angle) {
        super(angle);
        random = new Random(System.nanoTime());
    }

    @Override
    public void init() {
        nextStrike = System.currentTimeMillis();
        for (Participant participant : Participant.getAliveParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    regionWeatherEffect(player);
                }
            }
        }
    }

    @Override
    public void tick() {
        if (System.currentTimeMillis() < nextStrike) {
            return;
        }
        ArrayList<Participant> inRegion = new ArrayList<>();
        for (Participant participant : Participant.getAliveParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    inRegion.add(participant);
                }
            }
        }

        if (inRegion.isEmpty()) {
            return;
        }

        for (Participant participant : inRegion) {
            Player player = Bukkit.getPlayer(participant.getName());

            if (player != null && !participant.checkIfHasEffect(SGItemAction.SAFE_STRIKE)) {
                player.getWorld().strikeLightningEffect(player.getLocation());
                player.sendMessage("You were struck by lightning");
                player.damage(2.5);
            }
        }

        nextStrike = System.currentTimeMillis() + 5000;//5 seconds to next strike
    }

    public void regionWeatherEffect(Player player) {
        if (getActivated() != 1) {
            if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
                player.setPlayerWeather(WeatherType.CLEAR);
            }
            return;
        }
        if (player.getPlayerWeather() == WeatherType.CLEAR || player.getPlayerWeather() == null) {
            Participant participant = Participant.getParticipant(player.getName());
            if (!participant.isAlive()) {
                return;
            }
            player.setPlayerWeather(WeatherType.DOWNFALL);
        }
    }

    public void deactivate() {
        for (Participant participant : Participant.getAliveParticipants()) {
            Player player = Bukkit.getPlayer(participant.getName());
            if (player != null) {
                if (inRegion(player.getLocation())) {
                    if (player.getPlayerWeather() != WeatherType.CLEAR && player.getPlayerWeather() != null) {
                        player.setPlayerWeather(WeatherType.CLEAR);
                    }
                }
            }
        }
    }

    public String regionName() {
        return "Lightning";
    }

}
