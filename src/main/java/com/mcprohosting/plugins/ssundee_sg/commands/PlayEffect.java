package com.mcprohosting.plugins.ssundee_sg.commands;

import org.bukkit.Effect;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayEffect implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.playeffect")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }
        for (int x = -5; x < 5; x++) {
            for (int z = -5; z < 5; z++) {
                player.getWorld().playEffect(player.getLocation().add(x, 0, z), Effect.POTION_SWIRL, 0x0000FF);//This is spigot only :(
            }
        }
        return true;
    }

}
