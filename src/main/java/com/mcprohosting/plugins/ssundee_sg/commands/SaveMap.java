package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.utils.map.WorldManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SaveMap implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if ((sender instanceof Player) == false) {
            sender.sendMessage("You must be a player to use this command!");
            return true;
        }

        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.savemap")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }

        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.MAINTENANCE) {
            sender.sendMessage("You can only add maps while in maintenance mode!");
            return true;
        }

        if (DatabaseManager.containsMap(player.getWorld().getName())) {
            WorldManager.saveWorld(player.getWorld().getName());
            return true;
        }

        return true;
    }
}
