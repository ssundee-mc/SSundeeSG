package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Lobby implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to do this.");
            return true;
        }

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);


        try {
            out.writeUTF("Connect");
            out.writeUTF(SsundeeSG.grabConfig().settings_lobbyserver);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Player player = (Player) sender;

        player.sendPluginMessage(SsundeeSG.getPlugin(), "BungeeCord", b.toByteArray());

        sender.sendMessage(ChatColor.AQUA + "Connecting to lobby server...");
        return true;
    }
}
