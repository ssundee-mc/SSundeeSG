package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.utils.LocationUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.setspawn")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.MAINTENANCE) {
            sender.sendMessage("You can only set spawns while in maintenance mode!");
            return true;
        }
        if (args.length == 0) {
            if (!DatabaseManager.containsMap(player.getWorld().getName())) {
                sender.sendMessage("The map " + player.getWorld().getName() + " does not exist. Please add it using /addMap");
                return true;
            }
            int mapId = DatabaseManager.getMapId(player.getWorld().getName());
            if (DatabaseManager.containsSpawn(mapId, LocationUtils.LocationToString(player.getLocation()))) {
                sender.sendMessage("This spawn already exists!");
                return true;
            }
            DatabaseManager.addSpawn(mapId, LocationUtils.LocationToString(player.getLocation()));
            sender.sendMessage("Added a new spawn.");
            return true;
        }

        return false;
    }

}
