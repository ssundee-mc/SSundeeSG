package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.entities.Participant;
import com.mcprohosting.plugins.ssundee_sg.game.logic.Game;
import com.mcprohosting.plugins.ssundee_sg.utils.map.GameMap;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Vote implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 1) {
            return false;
        }

        Participant participant = Participant.getParticipant(sender.getName());
        if (participant.hasVoted()) {
            sender.sendMessage("You already voted for a map");
            return true;
        }

        String map = args[0].toLowerCase();
        GameMap gameMap = null;
        for (GameMap gm : Game.getMaps().keySet()) {
            if (gm.getMapName().equalsIgnoreCase(map)) {
                gameMap = gm;
                break;
            }
        }
        if (gameMap != null) {
            Game.getMaps().put(gameMap, Game.getMaps().get(gameMap) + 1);
        } else {
            sender.sendMessage("That map does not exist");
            return true;
        }

        participant.setVoted();
        sender.sendMessage("Thank you for voting!");
        return true;
    }
}
