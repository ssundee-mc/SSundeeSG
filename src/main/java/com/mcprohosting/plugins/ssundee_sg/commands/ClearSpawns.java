package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearSpawns implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.clearspawns")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.MAINTENANCE) {
            sender.sendMessage("You can only clear spawns while in maintenance mode!");
            return true;
        }
        if (!(args.length == 0)) {
            return false;
        }
        if (!DatabaseManager.containsMap(player.getWorld().getName())) {
            sender.sendMessage("The map " + player.getWorld().getName() + " does not exist. Please add it using /addMap");
            return true;
        }
        int mapId = DatabaseManager.getMapId(player.getWorld().getName());
        for (String location : DatabaseManager.getSpawns(mapId)) {
            DatabaseManager.deleteSpawn(mapId, location);
        }
        player.sendMessage("All spawns have been removed!");
        return true;
    }

}
