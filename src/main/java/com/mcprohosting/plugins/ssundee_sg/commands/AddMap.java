package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.utils.LocationUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddMap implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.addmap")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }

        if (args.length == 0) {
            if (containsMap(player)) {
                return true;
            }
            if (maintenanceModeEnabled(player) == false) {
                return true;
            }
            DatabaseManager.addMap(player.getWorld().getName(), LocationUtils.LocationToString(player.getLocation()));
            sender.sendMessage("The map " + player.getWorld().getName() + " has been added to the database");
            return true;
        } else if (args.length == 1) {
            if (containsMap(player)) {
                return true;
            }
            if (maintenanceModeEnabled(player) == false) {
                return true;
            }
            int radius = 0;

            try {
                radius = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                sender.sendMessage("You must enter a valid number.");
                return true;
            }

            if (radius <= 0) {
                sender.sendMessage("You must enter a number greater than 0.");
                return true;
            }

            DatabaseManager.addDeathmatchMap(player.getWorld().getName(),
                    LocationUtils.LocationToString(player.getLocation()),
                    radius);
            sender.sendMessage("The deathmatch map " + player.getWorld().getName() + " has been added to the database");
            return true;
        }

        return false;
    }

    private boolean maintenanceModeEnabled(Player sender) {
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.MAINTENANCE) {
            sender.sendMessage("You can only add maps while in maintenance mode!");
            return false;
        }

        return true;
    }

    private boolean containsMap(Player sender) {
        if (DatabaseManager.containsMap(sender.getWorld().getName()) ||
                DatabaseManager.containsDeathmatchMap(sender.getWorld().getName())) {
            sender.sendMessage("The map " + sender.getWorld().getName() + " already exists in the database");
            return true;
        }

        return false;
    }
}
