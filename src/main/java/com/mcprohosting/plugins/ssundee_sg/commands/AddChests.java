package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.utils.LocationUtils;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class AddChests implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.addchests")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.MAINTENANCE) {
            sender.sendMessage("You can only add chests while in maintenance mode!");
            return true;
        }
        if (DatabaseManager.containsMap(player.getWorld().getName()) == false) {
            sender.sendMessage("The map " + player.getWorld().getName() + " does not exist. Please add it using /addMap");
            return true;
        }
        int mapId = DatabaseManager.getMapId(player.getWorld().getName());
        Location location = player.getLocation();
        Chunk centerChunk = location.getChunk();
        ArrayList<Location> chestLocations = new ArrayList<>();
        for (int chunkX = -45; chunkX <= 45; chunkX++) {
            for (int chunkZ = -45; chunkZ <= 45; chunkZ++) {
                Chunk chunk = player.getWorld().getChunkAt(centerChunk.getX() + chunkX, centerChunk.getZ() + chunkZ);
                for (int x = 0; x < 16; x++) {
                    for (int y = 0; y < 128; y++) {
                        for (int z = 0; z < 16; z++) {
                            Block block = chunk.getBlock(x, y, z);
                            if (block.getType() == Material.CHEST) {
                                chestLocations.add(block.getLocation());
                            }
                        }
                    }
                }
            }
        }
        for (Location loc : chestLocations) {
            if (DatabaseManager.containsChest(mapId, LocationUtils.LocationToString(loc)) == false) {
                DatabaseManager.addChest(mapId, LocationUtils.LocationToString(loc));
            }
        }
        sender.sendMessage("Added " + chestLocations.size() + " chest(s) from this chunk.");
        return true;

    }
}
