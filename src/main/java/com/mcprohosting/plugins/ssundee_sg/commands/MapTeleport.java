package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MapTeleport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.mapteleport")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }
        if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.MAINTENANCE) {
            sender.sendMessage("You can only teleport to maps in maintenance mode!");
            return true;
        }
        if (args.length == 1) {
            String mapName = args[0];
            World world = null;
            for (World w : Bukkit.getWorlds()) {
                if (w.getName().startsWith(mapName)) {
                    world = w;
                    break;
                }
            }
            if (world == null) {
                player.sendMessage(ChatColor.RED + "The map " + mapName + " does not exist!");
                return true;
            }
            player.teleport(world.getSpawnLocation());
            player.sendMessage(ChatColor.GREEN + "You were teleported to " + mapName);
        } else {
            player.sendMessage(ChatColor.RED + "Usage: /mp [mapName]");
            return true;
        }
        return true;
    }
}
