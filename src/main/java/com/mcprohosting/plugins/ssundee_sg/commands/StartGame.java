package com.mcprohosting.plugins.ssundee_sg.commands;

import com.mcprohosting.plugins.ssundee_sg.SsundeeSG;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.Game;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.utils.map.GameMap;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartGame implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (!player.hasPermission("ssundeegames.startgame")) {
            sender.sendMessage("You do not have permission to use this command!");
            return true;
        }

        if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.MAINTENANCE) {
            sender.sendMessage("You cannot get out of maintenance mode. You must restart first.");
            return true;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("fs")) {
                if (SsundeeSG.getPlugin().getGame().getStatus() == GameState.INPROGRESS) {
                    sender.sendMessage("You can not start the game while it is in progress.");
                    return true;
                }
                sender.sendMessage("Forcing Start");
                SsundeeSG.getPlugin().getGame().forceStart();
                return true;
            } else if (args[0].equalsIgnoreCase("fd")) {
                if (SsundeeSG.getPlugin().getGame().getStatus() != GameState.INPROGRESS) {
                    sender.sendMessage("You can not for deathmatch while the game is not running.");
                    return true;
                }
                sender.sendMessage("Forcing Deathmatch");
                SsundeeSG.getPlugin().getGame().forceDeathMatch();
                return true;
            } else if (args[0].equalsIgnoreCase("mm")) {
                sender.sendMessage("Maintenance Mode Activated");
                SsundeeSG.getPlugin().getGame().setStatus(GameState.MAINTENANCE);
                SsundeeSG.getPlugin().getGame().setMap(null);
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "Usage: /startgame (fs/fd/mm)");
                return true;
            }
        }
        Game.getMaps().clear();
        for (GameMap map : DatabaseManager.getMaps()) {
            Game.getMaps().put(map, 0);
        }
        SsundeeSG.getPlugin().getGame().setStatus(GameState.POSTVOTING);
        return true;
    }
}
