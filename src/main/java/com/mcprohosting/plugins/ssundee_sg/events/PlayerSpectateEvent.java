package com.mcprohosting.plugins.ssundee_sg.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerSpectateEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    public PlayerSpectateEvent(Player who) {
        super(who);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
