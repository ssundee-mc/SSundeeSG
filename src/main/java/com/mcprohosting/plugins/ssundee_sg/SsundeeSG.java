package com.mcprohosting.plugins.ssundee_sg;

import com.mcprohosting.plugins.ssundee_sg.commands.*;
import com.mcprohosting.plugins.ssundee_sg.config.Config;
import com.mcprohosting.plugins.ssundee_sg.config.ItemConfig;
import com.mcprohosting.plugins.ssundee_sg.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_sg.game.logic.Game;
import com.mcprohosting.plugins.ssundee_sg.game.logic.GameState;
import com.mcprohosting.plugins.ssundee_sg.listeners.BlockListener;
import com.mcprohosting.plugins.ssundee_sg.listeners.EntityListener;
import com.mcprohosting.plugins.ssundee_sg.listeners.PlayerListener;
import com.mcprohosting.plugins.ssundee_sg.listeners.WorldListener;
import com.mcprohosting.plugins.ssundee_sg.utils.map.GameMap;
import com.mcprohosting.plugins.ssundee_sg.utils.server.UpdateTab;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

public class SsundeeSG extends JavaPlugin {

    private static SsundeeSG plugin;
    private static Config config;
    private static ItemConfig itemConfig;
    private static DatabaseManager databaseManager;
    private Game game;
    private UpdateTab tabUpdater;

    public void onEnable() {
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        // Make it so that this can be accessed statically
        plugin = this;

        // Initialize configuration
        config = new Config(this);
        itemConfig = new ItemConfig(this);
        initConfig();

        // Initialize database
        registerDatabase();
        config.setServerID();

        // Register commands
        registerCommands();

        // Register listeners
        registerListeners();

        // Start the game
        game = new Game();
        game.setStatus(GameState.LOBBY);
        for (GameMap map : DatabaseManager.getMaps()) {
            Game.getMaps().put(map, 0);
        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, game, 20, 20);
    }

    public void onDisable() {
        try {
            grabConfig().save();
        } catch (InvalidConfigurationException e) {
            getLogger().warning("Failed to save the config!");
        }
        try {
            grabItemConfig().save();
        } catch (InvalidConfigurationException e) {
            getLogger().warning("Failed to save the item config!");
        }
    }

    public Game getGame() {
        return game;
    }

    private void initConfig() {
        try {
            config.init();
        } catch (InvalidConfigurationException e) {
            getLogger().warning("Failed to initialize the config!");
        }
        try {
            itemConfig.init();
        } catch (InvalidConfigurationException e) {
            getLogger().warning("Failed to initialize the item config!");
        }
    }

    private void registerCommands() {
        getCommand("clearspawns").setExecutor(new ClearSpawns());
        getCommand("setspawn").setExecutor(new SetSpawn());
        getCommand("addchests").setExecutor(new AddChests());
        getCommand("addmap").setExecutor(new AddMap());
        getCommand("vote").setExecutor(new Vote());
        getCommand("startgame").setExecutor(new StartGame());
        getCommand("playeffect").setExecutor(new PlayEffect());
        getCommand("mtp").setExecutor(new MapTeleport());
        getCommand("lobby").setExecutor(new Lobby());
        getCommand("savemap").setExecutor(new SaveMap());
        getCommand("sgversion").setExecutor(new Version());
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new EntityListener(), this);
        Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
        Bukkit.getPluginManager().registerEvents(new WorldListener(), this);
    }

    private void registerDatabase() {
        databaseManager = new DatabaseManager();
        databaseManager.init();
    }

    public static SsundeeSG getPlugin() {
        return plugin;
    }

    public static ItemConfig grabItemConfig() {
        return itemConfig;
    }

    public static Config grabConfig() {
        return config;
    }

    public static DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

}
